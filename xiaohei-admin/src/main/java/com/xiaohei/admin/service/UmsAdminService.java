package com.xiaohei.admin.service;

import com.xiaohei.admin.dto.AdminLoginParam;
import com.xiaohei.admin.dto.AdminRegisterParam;
import com.xiaohei.admin.dto.UpdateAdminPasswordParam;
import com.xiaohei.mbg.model.UmsAdmin;
import com.xiaohei.mbg.model.UmsMenu;
import com.xiaohei.mbg.model.UmsResource;
import com.xiaohei.mbg.model.UmsRole;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface UmsAdminService {

    //注册
    UmsAdmin register(AdminRegisterParam registerParam);

    //根据用户名来查询角色
    UmsAdmin getAdminByUsername(String username);


    //登录
    String login(AdminLoginParam loginParam);

    //UserDetailsService 接口需要
    UserDetails loadUserByUsername(String username);

    //通过id来获取用户
    UmsAdmin getItem(Integer id);

    //更新
    int update(Integer id, UmsAdmin admin);

    int delete(Integer id);

    //给用户分配角色
    int updateRole(Integer adminId, List<Integer> roleIds);

    //获取改用户的全部角色
    List<UmsRole> getRoleList(Integer adminId);

    //根据用户id 获取他拥有的菜单权限
    List<UmsMenu> getMenuList(Integer adminId);

    //根据用户id 获取他拥有的资源权限
    List<UmsResource> getResourceList(Integer adminId);

    //修改密码
    int updatePassword(UpdateAdminPasswordParam passwordParam);

    //分页查询
    List<UmsAdmin> list(String keyword, Integer pageSize, Integer pageNum);


}
