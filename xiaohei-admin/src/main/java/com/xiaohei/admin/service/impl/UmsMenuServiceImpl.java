package com.xiaohei.admin.service.impl;

import com.github.pagehelper.PageHelper;
import com.xiaohei.admin.dto.UmsMenuNode;
import com.xiaohei.admin.service.UmsMenuService;
import com.xiaohei.mbg.mapper.UmsMenuMapper;
import com.xiaohei.mbg.model.UmsMenu;
import com.xiaohei.mbg.model.UmsMenuExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: UmsMenuServiceImpl
 * @Description:
 * @Author: xiaohei on Date:2022/6/21 13:27
 * @Version: 1.0
 */
@Service
public class UmsMenuServiceImpl implements UmsMenuService {

    private static final Logger log = LoggerFactory.getLogger(UmsMenuServiceImpl.class);

    @Autowired
    private UmsMenuMapper menuMapper;

    @Override
    public int create(UmsMenu UmsMenu) {
        UmsMenu.setCreateTime(new Date());
        updateLevel(UmsMenu);
        return menuMapper.insert(UmsMenu);
    }

    //修改菜单的层级, 在添加时需要判断
    private void updateLevel(UmsMenu UmsMenu) {
        //parent_id等于0 那么代表是没有父级菜单的
        if (UmsMenu.getParentId() == 0){
            UmsMenu.setLevel(0);
        }else{
            //有父级菜单, 那我们要拿到父级菜单
            UmsMenu parentMenu = menuMapper.selectByPrimaryKey(UmsMenu.getParentId());
            if (parentMenu != null){
                UmsMenu.setLevel(parentMenu.getLevel()+1);
            }else{
                UmsMenu.setLevel(0);
            }
        }
    }

    @Override
    public int update(Integer id, UmsMenu UmsMenu) {
        UmsMenu.setMenuId(id);
        return menuMapper.updateByPrimaryKeySelective(UmsMenu);
    }

    @Override
    public int delete(Integer id) {
        int count = menuMapper.deleteByPrimaryKey(id);
        UmsMenuExample example = new UmsMenuExample();
        example.createCriteria().andParentIdEqualTo(id);
        menuMapper.deleteByExample(example);
        return count;
    }

    @Override
    public UmsMenu getItem(Integer id) {
        return menuMapper.selectByPrimaryKey(id);
    }

    /**
     *通过parent_id 来实现分层查询
     *
     * @param parentId 父菜单id
     * @param pageSize 一页查询几条数据
     * @param pageNum  第几页
     * @return
     */
    @Override
    public List<UmsMenu> list(Integer parentId, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        UmsMenuExample example = new UmsMenuExample();
        example.createCriteria().andParentIdEqualTo(parentId);
        return menuMapper.selectByExample(example);
    }

    @Override
    public List<UmsMenuNode> treeList() {
        List<UmsMenu> menuList = menuMapper.selectByExample(new UmsMenuExample());
        List<UmsMenuNode> result = menuList.stream()
                .filter(menu -> menu.getParentId().equals(0))
                .map(menu -> coverMenuNode(menu, menuList)).collect(Collectors.toList());
        return result;
    }

    private UmsMenuNode coverMenuNode(UmsMenu UmsMenu, List<UmsMenu> menuList) {
        UmsMenuNode node = new UmsMenuNode();
        BeanUtils.copyProperties(UmsMenu,node);
        List<UmsMenuNode> children = menuList.stream()
                .filter(subMenu -> subMenu.getParentId().equals(UmsMenu.getMenuId()))
                //考虑到有多级的情况下, 如果只有二级, 不需要递归查询
                .map(subMean -> coverMenuNode(subMean, menuList)).collect(Collectors.toList());
        node.setChildren(children);
        return node;
    }
}
