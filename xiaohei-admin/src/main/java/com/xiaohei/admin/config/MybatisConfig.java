package com.xiaohei.admin.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @ClassName: MybatisConfig
 * @Description:
 * @Author: xiaohei on Date:2022/6/20 16:11
 * @Version: 1.0
 */
@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = {"com.xiaohei.mbg.mapper","com.xiaohei.admin.dao","com.xiaohei.quartz.dao"})
public class MybatisConfig {
}
