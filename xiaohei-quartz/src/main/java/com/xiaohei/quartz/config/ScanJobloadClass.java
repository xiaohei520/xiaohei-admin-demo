package com.xiaohei.quartz.config;

import com.xiaohei.mbg.mapper.SysQuartzClassMapper;
import com.xiaohei.mbg.model.SysQuartzClass;
import com.xiaohei.mbg.model.SysQuartzClassExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * @ClassName: ScanJobloadClass
 * @Description: 扫描job类加载器
 * @author 凌殇
 * @date 2022年07月13日 12:24
 * @Version: 1.0
 */
@Component
public class ScanJobloadClass{
    private static final String BASE_PACKAGE = "com.xiaohei.quartz.job";
    private static final String BASE_INTERFACE= "com.xiaohei.quartz.dto.BaseJob";
    private static final String RESOURCE_PATTERN = "/**/*.class";
    @Autowired
    private SysQuartzClassMapper quartzClassMapper;
    private static final Logger log = LoggerFactory.getLogger(ScanJobloadClass.class);


    @PostConstruct
    public void getJobClass(){
        log.info("开始加载job");
        //spring工具类，可以获取指定路径下的全部类
        long id = 0L;
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        try {
            String pattern = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                    ClassUtils.convertClassNameToResourcePath(BASE_PACKAGE) + RESOURCE_PATTERN;
            Resource[] resources = resourcePatternResolver.getResources(pattern);
            //MetadataReader 的工厂类
            MetadataReaderFactory readerfactory = new CachingMetadataReaderFactory(resourcePatternResolver);
            quartzClassMapper.deleteByExample(new SysQuartzClassExample());
            for (Resource resource : resources) {
                //用于读取类信息
                MetadataReader reader = readerfactory.getMetadataReader(resource);
                //扫描到的class
                String classname = reader.getClassMetadata().getClassName();
                Class<?> clazz = Class.forName(classname);

                Object obj = clazz.newInstance();
                Method getName = clazz.getDeclaredMethod("getName",null);
                //获取job类的name值
                String name = (String)getName.invoke(obj, null);

                Class<?>[] interfaces = clazz.getInterfaces();
                for (Class<?> anInterface : interfaces) {
                    //判断这个类是否实现了BASE_INTERFACE中变量的接口
                    if (anInterface.getName().equals(BASE_INTERFACE)) {
                        SysQuartzClass sysQuartzClass = new SysQuartzClass();
                        sysQuartzClass.setId(++id);
                        sysQuartzClass.setClassName(classname);
                        sysQuartzClass.setName(name);
                        sysQuartzClass.setdCreateTime(new Date());
                        sysQuartzClass.setDelFlag(false);
                        quartzClassMapper.insert(sysQuartzClass);
                    }
                }

            }
        } catch (IOException | ClassNotFoundException | NoSuchMethodException e) {
            log.error(e.getMessage());
        } catch (IllegalAccessException e) {
            log.error(e.getMessage());
        } catch (InstantiationException e) {
            log.error(e.getMessage());

        } catch (InvocationTargetException e) {
            log.error(e.getMessage());

        }
    }
}
