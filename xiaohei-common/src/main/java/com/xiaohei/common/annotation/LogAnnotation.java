package com.xiaohei.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName: LogAnnotation
 * @Description:
 * @Author: xiaohei on Date:2022/8/8 19:59
 * @Version: 1.0
 */
@Target(ElementType.METHOD) //在方法上使用
@Retention(RetentionPolicy.RUNTIME) //在运行是保存信息
public @interface  LogAnnotation {
    String value() default "";
}
