package com.xiaohei.security.common;

import org.springframework.security.access.ConfigAttribute;

import java.util.Map;

/**
 * @ClassName: DynamicSecurityService
 * @Description:
 * @Author: xiaohei on Date:2022/6/20 15:45
 * @Version: 1.0
 */
public interface DynamicSecurityService {
    //这个方法是用来获取接口权限
    //为了让里面的参数进行脱敏, 使用SpringSecurity中带的类
    Map<String, ConfigAttribute> loadDataSource();
}
