package com.xiaohei.admin.service.impl;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.xiaohei.admin.dto.UmsResourceNode;
import com.xiaohei.admin.service.UmsResourceService;
import com.xiaohei.mbg.mapper.UmsResourceCategoryMapper;
import com.xiaohei.mbg.mapper.UmsResourceMapper;
import com.xiaohei.mbg.model.UmsResource;
import com.xiaohei.mbg.model.UmsResourceCategory;
import com.xiaohei.mbg.model.UmsResourceCategoryExample;
import com.xiaohei.mbg.model.UmsResourceExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: UmsResourceServiceImpl
 * @Description:
 * @Author: xiaohei on Date:2022/6/20 21:54
 * @Version: 1.0
 */
@Service
public class UmsResourceServiceImpl implements UmsResourceService {

    private static final Logger log = LoggerFactory.getLogger(UmsResourceServiceImpl.class);

    @Autowired
    private UmsResourceMapper resourceMapper;

    @Autowired
    private UmsResourceCategoryMapper resourceCategoryMapper;

    @Override
    public int create(UmsResource UmsResource) {
        UmsResource.setCreateTime(new Date());
        return resourceMapper.insert(UmsResource);
    }

    @Override
    public int update(Integer id, UmsResource UmsResource) {
        UmsResource.setResourceId(id);
        return resourceMapper.updateByPrimaryKey(UmsResource);
    }

    @Override
    public UmsResource getItem(Integer id) {
        return resourceMapper.selectByPrimaryKey(id);
    }



    public int delete(Integer id) {
        return resourceMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<UmsResource> list(String nameKeyword, String urlKeyword,Integer categoryId, Integer pageSize, Integer pageNum) {
        //pageNum是成倍增长
        PageHelper.startPage(pageNum,pageSize);
        UmsResourceExample example = new UmsResourceExample();
        UmsResourceExample.Criteria criteria = example.createCriteria();
        if (StrUtil.isNotEmpty(nameKeyword)){
            criteria.andNameZhLike('%'+nameKeyword+'%');
        }
        if (StrUtil.isNotEmpty(urlKeyword)){
            criteria.andUrlLike('%'+urlKeyword+'%');
        }
        if (categoryId!=null && categoryId>0){
            criteria.andCategoryIdEqualTo(categoryId);
        }
        return resourceMapper.selectByExample(example);
    }

    @Override
    public List<UmsResource> listAll() {
        return resourceMapper.selectByExample(new UmsResourceExample());
    }

    @Override
    public List<UmsResourceNode> treeList() {
        List<UmsResourceCategory> resourceCategoryList = resourceCategoryMapper.selectByExample(new UmsResourceCategoryExample());
        List<UmsResourceNode> nodeList = new ArrayList<>();
        List<UmsResource> resourceList = listAll();
        for (UmsResourceCategory UmsResourceCategory : resourceCategoryList) {
            UmsResourceNode node = new UmsResourceNode();
            node.setName(UmsResourceCategory.getName());
            node.setCategoryId(UmsResourceCategory.getCategoryId());
            node.setCreateTiem(UmsResourceCategory.getCreateTiem());
            nodeList.add(node);
        }

        for (UmsResourceNode resourceNode : nodeList) {
            List<UmsResource> collect = resourceList.stream().filter(resource -> resource.getCategoryId().equals(resourceNode.getCategoryId())).collect(Collectors.toList());
            if (collect.size() > 0) {
                resourceNode.setChildren(collect);
            }
        }
        return nodeList;
    }
}
