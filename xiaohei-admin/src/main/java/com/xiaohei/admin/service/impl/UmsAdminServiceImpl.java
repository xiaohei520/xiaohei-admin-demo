package com.xiaohei.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.xiaohei.admin.bo.UmsAdminUserDetails;
import com.xiaohei.admin.service.UmsAdminService;
import com.xiaohei.mbg.model.*;
import com.xiaohei.admin.dao.UmsAdminRoleRelationDao;
import com.xiaohei.admin.dto.AdminLoginParam;
import com.xiaohei.admin.dto.AdminRegisterParam;
import com.xiaohei.admin.dto.UpdateAdminPasswordParam;
import com.xiaohei.common.exception.Asserts;
import com.xiaohei.mbg.mapper.UmsAdminMapper;
import com.xiaohei.mbg.mapper.UmsRoleAdminRelationMapper;
import com.xiaohei.security.utils.TokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;


import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName: com.xiaohei.mbg.model.UmsAdminServiceImpl
 * @Description:
 * @Author: xiaohei on Date:2022/6/20 16:28
 * @Version: 1.0
 */
@Component
public class UmsAdminServiceImpl implements UmsAdminService {

    private static final Logger log = LoggerFactory.getLogger(UmsAdminServiceImpl.class);
    @Autowired
    private UmsAdminMapper studentAdminMapper;

    @Autowired
    private UmsRoleAdminRelationMapper roleAdminRelationMapper;

    @Autowired
    private UmsAdminRoleRelationDao adminRoleRelationDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenUtils tokenUtils;

    @Override
    public UmsAdmin register(AdminRegisterParam registerParam) {
        UmsAdmin studentAdmin = new UmsAdmin();
        //把参数类中的信息复制到bojo类中
        BeanUtils.copyProperties(registerParam,studentAdmin);
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(studentAdmin.getUsername());
        //判断用户名是否已经存在了
        List<UmsAdmin> studentAdmins = studentAdminMapper.selectByExample(example);
        if (studentAdmins.size()>0){
            return null;
        }

        //此处要进行密码加密,后期加上
        setData(studentAdmin);
        studentAdmin.setPassword(passwordEncoder.encode(studentAdmin.getPassword()));
        this.studentAdminMapper.insert(studentAdmin);
        return studentAdmin;
    }

    @Override
    public UmsAdmin getAdminByUsername(String username) {
        UmsAdminExample blogAdminExample = new UmsAdminExample();
        blogAdminExample.createCriteria().andUsernameEqualTo(username);
        List<UmsAdmin> blogAdmins = studentAdminMapper.selectByExample(blogAdminExample);
        if (blogAdmins!=null && blogAdmins.size()>0){
            UmsAdmin studentAdmin = blogAdmins.get(0);
            return studentAdmin;
        }
        return null;
    }

    @Override
    public String login(AdminLoginParam loginParam) {
        String token = null;
        try {
            UserDetails userDetails = loadUserByUsername(loginParam.getUsername());

            if (!passwordEncoder.matches(loginParam.getPassword(), userDetails.getPassword())) {
                Asserts.fail("密码不正确! ! !");
            }
            if (!userDetails.isEnabled()) {
                Asserts.fail("账号已被禁用! ! !");
            }
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);

            log.debug("该用户的权限: {}",userDetails.getAuthorities());
            token = tokenUtils.createToken(loginParam.getUsername());
        }catch (AuthenticationException e) {
            log.warn("登录异常: {}", e.getMessage());
        }
        return token;
    }

    public UserDetails loadUserByUsername(String username) {
        UmsAdmin adminByUsername = getAdminByUsername(username);
        if (adminByUsername!=null){
            List<UmsResource> resourceList = getResourceList(adminByUsername.getAdminId());
            resourceList.removeAll(Collections.singleton(null));
            return new UmsAdminUserDetails(adminByUsername, resourceList);
        }
        throw new UsernameNotFoundException("用户名或密码错误");
    }

    @Override
    public UmsAdmin getItem(Integer id) {
        return studentAdminMapper.selectByPrimaryKey(id);
    }

    @Override
    public int update(Integer id, UmsAdmin admin) {
        admin.setAdminId(id);
        UmsAdmin studentAdmin = studentAdminMapper.selectByPrimaryKey(id);
        if (passwordEncoder.matches(admin.getPassword(), studentAdmin.getPassword())) {
            //与原加密密码相同的不需要修改
            admin.setPassword(null);
        }else{
            if (StrUtil.isEmpty(admin.getPassword())) {
                admin.setPassword(null);
            }else{
                admin.setPassword(passwordEncoder.encode(admin.getPassword()));
            }
        }
        int count = studentAdminMapper.updateByPrimaryKeySelective(admin);
        return count;
    }

    @Override
    public int delete(Integer id) {
        int count = studentAdminMapper.deleteByPrimaryKey(id);
        return count;
    }


    //添加角色
    @Override
    public int updateRole(Integer adminId, List<Integer> roleIds) {
        int count = roleIds == null ? 0 : roleIds.size();
        //先删除该用户之前的角色,在更新
        UmsRoleAdminRelationExample adminRoleRelationExample = new UmsRoleAdminRelationExample();
        adminRoleRelationExample.createCriteria().andAdminIdEqualTo(adminId);
        roleAdminRelationMapper.deleteByExample(adminRoleRelationExample);
        //建立新关系
        if (!CollectionUtils.isEmpty(roleIds)) {
            ArrayList<UmsRoleAdminRelation> list = new ArrayList<>();
            for (Integer roleId : roleIds) {
                UmsRoleAdminRelation relation = new UmsRoleAdminRelation();
                relation.setRoleId(roleId);
                relation.setAdminId(adminId);
                list.add(relation);
            }
            adminRoleRelationDao.insertList(list);
        }
        return count;
    }

    @Override
    public List<UmsRole> getRoleList(Integer adminId) {
        return adminRoleRelationDao.getRoleByAdminIdList(adminId);
    }

    @Override
    public List<UmsMenu> getMenuList(Integer adminId) {
        List<UmsRole> roleByAdminIdList = adminRoleRelationDao.getRoleByAdminIdList(adminId);
        roleByAdminIdList.removeAll(Collections.singleton(null));
        return roleByAdminIdList;
    }

    @Override
    public List<UmsResource> getResourceList(Integer adminId) {
        List<UmsMenu> menuByAdminIdList = adminRoleRelationDao.getMenuByAdminIdList(adminId);
        menuByAdminIdList.removeAll(Collections.singleton(null));
        return menuByAdminIdList;
    }

    @Override
    public int updatePassword(UpdateAdminPasswordParam passwordParam) {
        if (StrUtil.isEmpty(passwordParam.getUsername())
                ||StrUtil.isEmpty(passwordParam.getOldPassword())
                ||StrUtil.isEmpty(passwordParam.getNewPassword())) {
            return -1;
        }
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(passwordParam.getUsername());
        List<UmsAdmin> adminList = studentAdminMapper.selectByExample(example);
        if (CollUtil.isEmpty(adminList)) {
            return -2;
        }
        UmsAdmin admin = adminList.get(0);
        if (!passwordEncoder.matches(passwordParam.getOldPassword(), admin.getPassword())) {
            return -3;
        }
        admin.setPassword(passwordEncoder.encode(passwordParam.getNewPassword()));
        studentAdminMapper.updateByPrimaryKey(admin);
        return 1;
    }

    @Override
    public List<UmsAdmin> list(String keyword, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        UmsAdminExample example = new UmsAdminExample();
        UmsAdminExample.Criteria criteria = example.createCriteria();
        if (StrUtil.isNotEmpty(keyword)) {
            criteria.andUsernameLike("%" + keyword + "%");
            example.or(example.createCriteria().andNickNameLike("%" + keyword + "%"));
        }
        return studentAdminMapper.selectByExample(example);
    }

    private void setData(UmsAdmin studentAdmin) {
        studentAdmin.setCreateTime(new Date());
        studentAdmin.setUpdateTime(new Date());
    }
}
