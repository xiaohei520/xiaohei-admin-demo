package com.xiaohei.mbg.mapper;

import com.xiaohei.mbg.model.SysQuartz;
import com.xiaohei.mbg.model.SysQuartzExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysQuartzMapper {
    long countByExample(SysQuartzExample example);

    int deleteByExample(SysQuartzExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SysQuartz record);

    int insertSelective(SysQuartz record);

    List<SysQuartz> selectByExample(SysQuartzExample example);

    SysQuartz selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SysQuartz record, @Param("example") SysQuartzExample example);

    int updateByExample(@Param("record") SysQuartz record, @Param("example") SysQuartzExample example);

    int updateByPrimaryKeySelective(SysQuartz record);

    int updateByPrimaryKey(SysQuartz record);
}