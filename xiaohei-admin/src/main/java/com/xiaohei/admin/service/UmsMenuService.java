package com.xiaohei.admin.service;

import com.xiaohei.admin.dto.UmsMenuNode;
import com.xiaohei.mbg.model.UmsMenu;

import java.util.List;

public interface UmsMenuService {
    int create(UmsMenu UmsMenu);

    int update(Integer id, UmsMenu UmsMenu);

    int delete(Integer id);

    UmsMenu getItem(Integer id);


    //列表显示
    List<UmsMenu> list(Integer parentId, Integer pageSize, Integer pageNum);

    //树形结构查询
    List<UmsMenuNode> treeList();
}
