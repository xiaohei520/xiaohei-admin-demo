package com.xiaohei.admin.bo;

import com.xiaohei.mbg.model.UmsAdmin;
import com.xiaohei.mbg.model.UmsResource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: StudentAdminUserDetails
 * @Description:
 * @Author: xiaohei on Date:2022/6/25 17:13
 * @Version: 1.0
 */
public class UmsAdminUserDetails implements UserDetails {

    private UmsAdmin studentAdmin;
    private List<UmsResource> resourceList;

    public UmsAdminUserDetails(UmsAdmin studentAdmin, List<UmsResource> resourceList) {
        this.studentAdmin = studentAdmin;
        this.resourceList = resourceList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return resourceList.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return this.studentAdmin.getPassword();
    }

    @Override
    public String getUsername() {
        return this.studentAdmin.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.studentAdmin.getStatus().equals(1);
    }
}
