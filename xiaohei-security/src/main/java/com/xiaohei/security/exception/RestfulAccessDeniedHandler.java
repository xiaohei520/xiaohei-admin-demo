package com.xiaohei.security.exception;

import com.xiaohei.common.utils.Result;
import com.xiaohei.common.utils.ResultCodeEnum;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: ReestfulAccessDeniedHandler
 * @Description: 当访问接口没有权限, 自定义返回结果
 * @Author: xiaohei on Date:2022/6/20 15:58
 * @Version: 1.0
 */
@Component
public class RestfulAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(Result.build("没有权限", ResultCodeEnum.PERMISSION));
        response.getWriter().flush();
    }
}
