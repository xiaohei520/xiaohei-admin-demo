package com.xiaohei.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: StringAddressToUtils
 * @Description:
 * @Author: xiaohei on Date:2022/8/8 21:17
 * @Version: 1.0
 */
public class StringAddressToUtils {
    public static final int StringToAddress(String address){
        if (!isIPv4Address(address)){
            return 0;
        }
        int ip = 0;
        String[] split =address.split("\\.");
        for (int i = 0; i < split.length; i++) {
            ip += Integer.parseInt(split[i] ) << (8 * (3-i));
        }
        return ip;
    }

    /**
     * 判断是否为ipv4地址
     * @param ipv4Addr
     * @return
     */
    private static boolean isIPv4Address(String ipv4Addr) {
        String lower = "(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])"; // 0-255的数字
        String regex = lower + "(\\." + lower + "){3}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(ipv4Addr);
        return matcher.matches();
    }

    public static final String StringToAddress(int address){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            sb.append((int)address >> 8 * (3-i) & 0xff);
            if (i != 3){
                sb.append(".");
            }
        }
        return sb.toString();
    }
}
