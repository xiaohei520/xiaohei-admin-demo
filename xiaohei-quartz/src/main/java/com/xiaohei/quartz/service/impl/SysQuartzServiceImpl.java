package com.xiaohei.quartz.service.impl;

import com.github.pagehelper.PageHelper;
import com.xiaohei.common.exception.ApiException;
import com.xiaohei.common.utils.IsNullUtils;
import com.xiaohei.common.utils.Result;
import com.xiaohei.common.utils.ResultCodeEnum;
import com.xiaohei.mbg.mapper.SysQuartzMapper;
import com.xiaohei.mbg.model.SysQuartz;
import com.xiaohei.mbg.model.SysQuartzExample;
import com.xiaohei.quartz.dao.SysQuartzDao;
import com.xiaohei.quartz.service.SysQuartzService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: SysQuartzServiceImpl
 * @Description: 定时任务类
 * @Author: xiaohei on Date:2022/7/11 16:13
 * @Version: 1.0
 */
@Service
public class SysQuartzServiceImpl implements SysQuartzService {

    private static final Logger log = LoggerFactory.getLogger(SysQuartzServiceImpl.class);

    @Autowired
    private SysQuartzMapper quartzMapper;

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private SysQuartzDao quartzDao;

    @Override
    public Result add(SysQuartz sysQuartz,String createName) {
        SysQuartzExample example = new SysQuartzExample();
        example.createCriteria().andClassNameEqualTo(sysQuartz.getClassName());
        sysQuartz.setDelFlag(false);
        List<SysQuartz> quartzList = quartzMapper.selectByExample(example);
        if (quartzList.size()>0) {
            return Result.build(ResultCodeEnum.QUARTZ_ERROR.getCode(), ResultCodeEnum.QUARTZ_ERROR.getMessage());
        }
        sysQuartz.setCreateTime(new Date());
        sysQuartz.setCreateUser(createName);
        quartzMapper.insert(sysQuartz);
        //启动状态, 0对应false
        if (!sysQuartz.getStatus()) {
            this.schedulerAdd(sysQuartz.getClassName().trim(),sysQuartz.getCronExpression().trim(),sysQuartz.getParam());
        }
        return Result.ok("新增定时任务成功");
    }

    @Override
    public void schedulerAdd(String className, String cronExpression, String param) {
        try {
            //先删除在添加
            schedulerDelete(className);
            //启动调度器
            scheduler.start();
            // 构建job信息
            JobDetail jobDetail = JobBuilder.newJob(getClass(className).getClass()).withIdentity(className).usingJobData("param", param).build();
            // 表达式调度构建器(即任务执行的时间)
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
            // 按新的cronExpression表达式构建一个新的trigger
            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(className).withSchedule(scheduleBuilder).build();
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            log.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());

        }
    }


    //删除任务类
    @Override
    public void schedulerDelete(String className) {
        try {
            scheduler.pauseTrigger(TriggerKey.triggerKey(className));
            scheduler.unscheduleJob(TriggerKey.triggerKey(className));
            scheduler.deleteJob(JobKey.jobKey(className));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    //项目启动时启动quartz
    @PostConstruct
    @Override
    public void schedulerStart() {
        //防止过度调用api定时任务多个
        try {
            scheduler.clear();
        } catch (SchedulerException e) {
            log.error("scheduler清空定时任务失败: {}",e.getMessage());
        }
        SysQuartzExample example = new SysQuartzExample();
        SysQuartzExample.Criteria criteria = example.createCriteria();
        //启用且未删除状态
        criteria.andStatusEqualTo(false);
        criteria.andDelFlagEqualTo(false);
        List<SysQuartz> quartzList = quartzMapper.selectByExample(example);
        for (SysQuartz sysQuartz : quartzList) {
            // 添加定时任务
            schedulerAdd(sysQuartz.getClassName().trim(), sysQuartz.getCronExpression().trim(), sysQuartz.getParam());
        }
        log.info("开启了扫描数据库中的定时任务定时任务");
    }

    @Override
    public int delete(String className) {
        SysQuartzExample example = new SysQuartzExample();
        example.createCriteria().andClassNameEqualTo(className);
        int delete = quartzMapper.deleteByExample(example);
        return delete;
    }

    @Override
    public void schedulerStop(String className) {
        try {
            scheduler.pauseJob(JobKey.jobKey(className));
        } catch (SchedulerException e) {
            log.error("停止指定定时任务异常: {}",e.getMessage());
        }
    }

    @Override
    public void schedulerStart(String className) {
        try {
            scheduler.resumeJob(JobKey.jobKey(className));
        } catch (SchedulerException e) {
            log.error("启动指定定时任务异常: {}",e.getMessage());
        }
    }

    private static Job getClass(String className) throws Exception {
        Class<?> class1 = Class.forName(className);
        return (Job) class1.newInstance();
    }

    @Override
    public List<SysQuartz> select(SysQuartz sysQuartz) {
        SysQuartzExample example = new SysQuartzExample();
        SysQuartzExample.Criteria criteria = example.createCriteria();
        if (!IsNullUtils.isNull(sysQuartz.getClassName())){
            criteria.andClassNameEqualTo(sysQuartz.getClassName());
        }
        if (!IsNullUtils.isNull(sysQuartz.getCronExpression())){
            criteria.andCronExpressionEqualTo(sysQuartz.getCronExpression());
        }
        if (!IsNullUtils.isNull(sysQuartz.getParam())){
            criteria.andParamEqualTo(sysQuartz.getParam());
        }
        if (!IsNullUtils.isNull(sysQuartz.getDescription())){
            criteria.andDescriptionEqualTo(sysQuartz.getDescription());
        }
        criteria.andStatusEqualTo(sysQuartz.getStatus());
        return quartzMapper.selectByExample(example);
    }

    @Override
    public List<SysQuartz> selectByClassName(String ClassName) {
        SysQuartzExample example = new SysQuartzExample();
        example.createCriteria().andClassNameEqualTo(ClassName);
        return quartzMapper.selectByExample(example);
    }

    @Override
    public List<SysQuartz> selectByFuzzy(SysQuartz sysQuartz, Integer pageSize, Integer pageNum) {
        PageHelper.offsetPage(pageNum - 1,pageSize);
        SysQuartzExample example = new SysQuartzExample();
        SysQuartzExample.Criteria criteria = example.createCriteria();
        if (!IsNullUtils.isNull(sysQuartz.getClassName())){
            criteria.andClassNameLike("%" + sysQuartz.getClassName() + "%");
        }
        if (!IsNullUtils.isNull(sysQuartz.getCronExpression())){
            criteria.andCronExpressionLike("%" + sysQuartz.getCronExpression() + "%");
        }
        if (!IsNullUtils.isNull(sysQuartz.getParam())){
            criteria.andParamLike("%" + sysQuartz.getParam() + "%");
        }
        if (!IsNullUtils.isNull(sysQuartz.getDescription())){
            criteria.andDescriptionLike("%" + sysQuartz.getDescription() + "%");
        }
        criteria.andStatusEqualTo(sysQuartz.getStatus());
        return quartzMapper.selectByExample(example);
    }

    @Override
    public int update(SysQuartz sysQuartz) {
        schedulerAdd(sysQuartz.getClassName(), sysQuartz.getCronExpression(), sysQuartz.getParam());
        return quartzMapper.updateByPrimaryKeySelective(sysQuartz);
    }

    @Override
    public int updateStatus(String className, Boolean status) {
        return quartzDao.updateStatus(className,status);
    }

    @Override
    public void isValidExpression(String cron) {
        boolean validExpression = CronExpression.isValidExpression(cron);
        if (!validExpression){
            throw new ApiException(ResultCodeEnum.QUARTZ_CRON_ERROR);
        }
    }
}
