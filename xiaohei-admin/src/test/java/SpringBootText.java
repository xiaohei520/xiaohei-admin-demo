import com.xiaohei.admin.UmsAdminApplication;
import com.xiaohei.admin.dto.AdminLoginParam;
import com.xiaohei.admin.dto.AdminRegisterParam;

import com.xiaohei.admin.service.UmsAdminService;
import com.xiaohei.admin.service.UmsResourceService;
import com.xiaohei.mbg.model.UmsAdmin;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName: SpringBootText
 * @Description:
 * @Author: xiaohei on Date:2022/6/20 18:47
 * @Version: 1.0
 */
@SpringBootTest(classes = UmsAdminApplication.class)
@RunWith(SpringRunner.class)
public class SpringBootText {
    private static final Logger log = LoggerFactory.getLogger(SpringBootText.class);

    @Autowired
    private UmsAdminService studentAdminService;

    @Autowired
    private UmsResourceService studentResourceService;


    @Test
    public void printValue() {

    }

    @Test
    public void resourceTest(){
        //StudentResource studentResource = new StudentResource();
        //studentResource.setCreateTime(new Date());
        //studentResource.setDescription("这个是admin资源");
        //studentResource.setName("admin");
        //studentResource.setNameZh("用户管理");
        //studentResource.setUrl("/admin/**");
        //for (int i = 0; i < 10; i++) {
        //    studentResourceService.create(studentResource);
        //}

        //StudentResource item = studentResourceService.getItem(1);
        //System.out.println(item);

        //studentResourceService.delete(2);
        //List<StudentResource> list = studentResourceService.list(null, null, 5, 2);
        //System.out.println(list);
        //List<StudentResource> studentResources = studentResourceService.listAll();
        //System.out.println(studentResources);
        studentResourceService.treeList();
    }

    @Test
    public void getStudentByUsername(){
        UmsAdmin user3 = studentAdminService.getAdminByUsername("user3");
    }

    //注册测试
    @Test
    public void register() throws Exception {
        AdminRegisterParam adminRegisterParam = new AdminRegisterParam();
        adminRegisterParam.setIcon("wewae");
        adminRegisterParam.setUsername("user2");
        adminRegisterParam.setPassword("123456");
        adminRegisterParam.setNickName("小黑");
        adminRegisterParam.setEmail("20320302@@@@");
        studentAdminService.register(adminRegisterParam);
    }

    @Test
    public void login(){
        AdminLoginParam admin = new AdminLoginParam();
        admin.setUsername("user3");
        admin.setPassword("123456");
        String login = studentAdminService.login(admin);
        log.info(login);

    }

}
