package com.xiaohei.security.utils;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public class TokenUtils {
    private static final Logger log = LoggerFactory.getLogger(TokenUtils.class);
    //定义token的header的名称
    @Value("${jwt.tokenHeader}")
    public  String TOKEN_HEADER;
    //按照jwt的规定使用Bearer token 来发送
    @Value("${jwt.tokenHead}")
    public String TOKEN_PREFIX ;

    //加密所需的加密密钥
    @Value("${jwt.secret}")
    private  String SECRET;
    private  String ISS = "echisan";

    // 过期时间是3600秒，既是1个小时
    @Value("${jwt.expiration}")
    private long EXPIRATION;

    // 选择了记住我之后的过期时间为7天
    private static final long EXPIRATION_REMEMBER = 604800L;
    // 创建token
    public  String createToken(String username) {
        log.info("{}用户登录, 开始创建token信息",username);
        //long expiration = isRememberMe ? EXPIRATION_REMEMBER : EXPIRATION;
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .setIssuer(ISS)
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION  * 1000))
                .compact();
    }

    //从token中获取用户名
    public  String getUsername(String token){
        return getTokenBody(token).getSubject();
    }

//    //获取用户角色
//    public static String getUserRole(String token){
//        return (String)getTokenBody(token).get(ROLE_CLAIMS);
//    }

    //是否过期
    public  boolean isExpiration(String token){
        try{
            //获取过期时间在对比现在的时间
            return getTokenBody(token).getExpiration().before(new Date());
        }catch (ExpiredJwtException e){
            return true;  //返回true为过期了
        }
    }


    //解析token，并拿到其中带的信息
    public  Claims getTokenBody(String token){
        return Jwts.parser()
                .setSigningKey(SECRET)  //加密于解密的对应字符串要一致
                .parseClaimsJws(token)  //加密之后的token值
                .getBody();
    }
}