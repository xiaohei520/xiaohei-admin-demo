package com.xiaohei.admin.controller;

import com.xiaohei.admin.dto.QuartzParam;
import com.xiaohei.common.api.CommonPage;
import com.xiaohei.common.utils.Result;
import com.xiaohei.mbg.model.SysQuartz;
import com.xiaohei.mbg.model.SysQuartzClass;
import com.xiaohei.quartz.service.SysQuartzClassService;
import com.xiaohei.quartz.service.SysQuartzService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * @ClassName: SysQuartzController
 * @Description:
 * @Author: xiaohei on Date:2022/7/11 17:31
 * @Version: 1.0
 */
@RestController
@RequestMapping("/quartz")
public class SysQuartzController {

    @Autowired
    private SysQuartzService sysQuartzService;

    @Autowired
    SysQuartzClassService sysQuartzClassService;

    @ApiOperation(value = "添加定时任务")
    @PostMapping(value = "/create")
    public Result create(QuartzParam quartzParam, Principal principal) {
        SysQuartz sysQuartz = new SysQuartz();
        BeanUtils.copyProperties(quartzParam,sysQuartz);
        //判断是否是cron格式
        sysQuartzService.isValidExpression(sysQuartz.getCronExpression().trim());
        return sysQuartzService.add(sysQuartz,principal.getName());
    }

    @ApiOperation(value = "删除定时任务")
    @PostMapping(value = "/delete")
    public Result delete(@RequestParam(value = "className") String className) {
        int delete = sysQuartzService.delete(className);
        if (delete == 0) {
            return Result.fail("删除失败");
        }
        return Result.ok("删除成功!共删除" + delete + "条!");
    }

    @ApiOperation(value = "更新定时任务")
    @PostMapping(value = "/update")
    public Result update(@RequestBody SysQuartz sysQuartz) {
        //判断是否是cron格式
        sysQuartzService.isValidExpression(sysQuartz.getCronExpression().trim());
        int update = sysQuartzService.update(sysQuartz);
        if (update == 0) {
            return Result.fail("更新失败");
        }
        return Result.ok("更新成功");
    }

    @ApiOperation(value = "更新定时任务的状态")
    @PostMapping(value = "/updateByStatus")
    public Result updateByStatus(@RequestParam(value = "className") String className,
                                 @RequestParam(value = "status", defaultValue = "false") Boolean status) {
        int update = sysQuartzService.updateStatus(className,status);
        if (update == 0) {
            return Result.fail("更新失败");
        }
        return Result.ok("更新成功");
    }

    @ApiOperation(value = "查询指定定时任务")
    @PostMapping(value = "/select")
    public Result select(@RequestBody QuartzParam quartzParam) {
        SysQuartz sysQuartz = new SysQuartz();
        BeanUtils.copyProperties(quartzParam,sysQuartz);
        List<SysQuartz> list = sysQuartzService.select(sysQuartz);
        return Result.ok(CommonPage.restPage(list));
    }

    @ApiOperation(value = "查询指定定时任务")
    @PostMapping(value = "/selectByClassName")
    public Result selectByClassName(@RequestParam(value = "className") String className) {
        List<SysQuartz> list = sysQuartzService.selectByClassName(className);
        return Result.ok(CommonPage.restPage(list));
    }

    @ApiOperation(value = "模糊查询定时任务")
    @PostMapping(value = "/selectByFuzzy")
    public Result selectByFuzzy(@RequestBody QuartzParam quartzParam,
                         @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        SysQuartz sysQuartz = new SysQuartz();
        BeanUtils.copyProperties(quartzParam,sysQuartz);
        List<SysQuartz> list = sysQuartzService.selectByFuzzy(sysQuartz,pageSize,pageNum);
        return Result.ok(CommonPage.restPage(list));
    }

    @ApiOperation(value = "停止指定定时任务")
    @PostMapping("/schedulerStop")
    public Result schedulerStop(@RequestParam(value = "name") String name){
        //先使用前端传入Name的映射名字查找到对应的className，再使用className查找对应的定时任务添加到scheduler
        List<SysQuartzClass> list = sysQuartzClassService.select(name);
        if (list.isEmpty() && list.size() == 0){
            return Result.ok("未找到合适的定时任务");
        }
        List<SysQuartz> sysQuartz = sysQuartzService.selectByClassName(list.get(0).getClassName());
        sysQuartzService.schedulerStop(list.get(0).getClassName());
        sysQuartzService.updateStatus(list.get(0).getClassName(),true);
        return Result.ok("停止任务：" + name);
    }

    @ApiOperation(value = "启动指定定时任务")
    @PostMapping("/schedulerStart")
    public Result schedulerStart(@RequestParam(value = "name") String name){
        //先使用前端传入Name的映射名字查找到对应的className，再使用className查找对应的定时任务添加到scheduler
        List<SysQuartzClass> list = sysQuartzClassService.select(name);
        if (list.isEmpty() && list.size() == 0){
            return Result.ok("未找到合适的定时任务");
        }
        List<SysQuartz> sysQuartz = sysQuartzService.selectByClassName(list.get(0).getClassName());
        sysQuartzService.schedulerAdd(sysQuartz.get(0).getClassName(),sysQuartz.get(0).getCronExpression(),sysQuartz.get(0).getParam());
        updateByStatus(sysQuartz.get(0).getClassName(),false);
        return Result.ok("启动任务：" + name);
    }

    @ApiOperation(value = "查询指定name定时任务类型数据库映射")
    @PostMapping("/selectByName")
    public Result select(@RequestParam(value = "name") String name){
        List<SysQuartzClass> list = sysQuartzClassService.select(name);
        return Result.ok(list);
    }

    @ApiOperation(value = "查询全部定时任务类型数据库映射")
    @PostMapping("/selectAllByClass")
    public Result selectAllByClass(){
        List<SysQuartzClass> list = sysQuartzClassService.selectAll();
        return Result.ok(list);
    }

    @ApiOperation(value = "模糊查询定时任务类型数据库映射")
    @PostMapping("/selectByClass")
    public Result selectByClass(@RequestParam String name){
        List<SysQuartzClass> list = sysQuartzClassService.selectByFuzzy(name);
        return Result.ok(list);
    }



}
