package com.xiaohei.admin.dto;

import com.xiaohei.mbg.model.UmsMenu;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @ClassName: StudentMenuNode
 * @Description:
 * @Author: xiaohei on Date:2022/6/21 13:59
 * @Version: 1.0
 */
@Getter
@Setter
public class UmsMenuNode extends UmsMenu {
    @ApiModelProperty(value = "子级菜单")
    private List<UmsMenuNode> children;
}
