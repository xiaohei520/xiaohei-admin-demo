package com.xiaohei.security.filter;

import com.xiaohei.security.utils.TokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: JWTAuthorizationTokenFilter
 * @Description:
 * @Author: xiaohei on Date:2022/4/23 17:03
 * @Version: 1.0
 */
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(JwtAuthenticationTokenFilter.class);

    @Autowired(required = false)
    private UserDetailsService userDetailsService;

    @Autowired
    private TokenUtils tokenUtils;
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;

//    public JwtAuthenticationTokenFilter(AuthenticationManager authenticationManager) {
//        super(authenticationManager);
////        this.userDetailsService = userDetailsService;
//    }


    /**
     * 这个方法在用户访问带权限的方法是会来调用
     *  这个方法就是识别Token 并吧Token解析获取到用户名
     *  通过用户名来获取用户的权限
     *  并调用AccessDecisionManager中的decide方法来进行
     *  判断这个用户是否有执行权限
     */

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //从headr获取token
        String token = request.getHeader(this.tokenHeader);
        if (token!=null && !token.equals("")&&token.startsWith(this.tokenHead)){
            token = token.replace(this.tokenHead, "");
            if (!tokenUtils.isExpiration(token)) {
                String username = tokenUtils.getUsername(token);
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(request,response);
    }
}
