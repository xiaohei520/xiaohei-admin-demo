package com.xiaohei.security.config;

import com.xiaohei.security.exception.RestAuthenticationEntryPoint;
import com.xiaohei.security.exception.RestfulAccessDeniedHandler;
import com.xiaohei.security.filter.CustomerAccessDecisionManger;
import com.xiaohei.security.filter.CustomerFilterInvocationSecurityMetadataSource;
import com.xiaohei.security.filter.JwtAuthenticationTokenFilter;
import com.xiaohei.security.utils.TokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @ClassName: SecurityConfig
 * @Description:
 * @Author: xiaohei on Date:2022/4/23 17:22
 * @Version: 1.0
 */

public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger log = LoggerFactory.getLogger(SecurityConfig.class);
    @Autowired(required = false)
    private UserDetailsService userDetailsService;

    @Autowired(required = false)
    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;
    @Autowired(required = false)
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
                    @Override
                    public <O extends FilterSecurityInterceptor> O postProcess(O object) {
                        object.setSecurityMetadataSource(customerFilterInvocationSecurityMetadataSource());
                        object.setAccessDecisionManager(customerAccessDecisionManger());
                        return object;
                    }
                })

                // 除了上面外的所有请求全部需要认证
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS)//跨域请求会先进行一次options请求
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
//                .addFilter(new JWTAuthorizationTokenFilter(authenticationManager()))
                .httpBasic().and()
                // 不需要session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling();

        http.addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        //添加自定义未授权和未登录结果返回
        http.exceptionHandling()
                .accessDeniedHandler(restfulAccessDeniedHandler)
                .authenticationEntryPoint(restAuthenticationEntryPoint);

    }

    //不进行认证的路径，可以直接访问
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.GET,
                "/api/**",
                "/*.html",
                "/favicon.ico",
                "/**/*.html",
                "/**/*.css",
                "/**/*.js",
                "/swagger-resources/**",
                "/v2/api-docs/**");
        web.ignoring().antMatchers(HttpMethod.POST,
                "/admin/login", "/admin/register");
    }

    //不在这里注入容器的话 Filter不能自动装配@Autowired
    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter(){
        return new JwtAuthenticationTokenFilter();
    }

    @Bean
    public CustomerFilterInvocationSecurityMetadataSource customerFilterInvocationSecurityMetadataSource(){
        return new CustomerFilterInvocationSecurityMetadataSource();
    }

    @Bean
    public CustomerAccessDecisionManger customerAccessDecisionManger(){
        return new CustomerAccessDecisionManger();
    }

    @Bean
    public TokenUtils getTokenUtils(){
        return new TokenUtils();
    }
}
