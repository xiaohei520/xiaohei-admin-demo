package com.xiaohei.common.aspect;

import com.alibaba.fastjson.JSONObject;
import com.xiaohei.common.annotation.PreventDuplication;
import com.xiaohei.common.exception.ApiException;
import com.xiaohei.common.exception.Asserts;
import com.xiaohei.common.service.RedisService;
import com.xiaohei.common.utils.ResultCodeEnum;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.script.DigestUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * @ClassName: PreventDuplicationAspect
 * @Description:
 * @Author: xiaohei on Date:2022/8/9 15:00
 * @Version: 1.0
 */
@Aspect
@Component
public class PreventDuplicationAspect {
    @Autowired
    private RedisService redisService;

    private static final Logger log = LoggerFactory.getLogger(PreventDuplicationAspect.class);

    @Value("${jwt.tokenHeader}")
    public  String TOKEN_HEADER;

    /**
     * 定义切点
     */
    @Pointcut("@annotation(com.xiaohei.common.annotation.PreventDuplication)")
    public void preventDuplication(){ }

    /**
     * 环绕通知 （可以控制目标方法前中后期执行操作，目标方法执行前后分别执行一些代码）
     *
     * @param joinPoint
     * @return
     */
    @Around("preventDuplication()")
    public Object before(ProceedingJoinPoint joinPoint){
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        if (request == null){
            Asserts.fail("request cannot be null .");
        }

        //获取执行方法
        Method method = ((MethodSignature)joinPoint.getSignature()).getMethod();
        //获取防重复提交注解
        PreventDuplication annotation = method.getAnnotation(PreventDuplication.class);

        //获取token以及方法标记，生成redisKey和redisValue
        String token = request.getHeader(TOKEN_HEADER);
        if (token == null){
            token = "login-register";
        }
        String redisKey = token + getMethodSign(method, joinPoint.getArgs());
        String redisValue = annotation.value().concat("submit duplication");

        if (!redisService.hasKey(redisKey)){
            //设置重复提交即使标记( 前置通知 )
            redisService.set(redisKey,redisValue);
            redisService.expire(redisKey, annotation.expireSeconds());
            try {
                //正常执行方法并返回
                return joinPoint.proceed();
            } catch (Throwable throwable) {
                //保证异常时删除key
                redisService.del(redisKey);
                log.error("redis的错误: {}",throwable.getLocalizedMessage());
                throw new ApiException(ResultCodeEnum.REQUEST_ERROR);
            }
        }else{
            throw new ApiException(ResultCodeEnum.REQUEST_ERROR);
        }
    }

    /**
     * 生成方法标记：采用数字签名算法SHA1对方法签名字符串加签
     *
     * @param method
     * @param args
     * @return
     */
    private String getMethodSign(Method method, Object... args) {
        StringBuilder sb = new StringBuilder(method.toString());
        for (Object arg : args) {
            sb.append(toString(arg));
        }
        return DigestUtils.sha1DigestAsHex(sb.toString());
    }

    private String toString(Object arg) {
        if (Objects.isNull(arg)) {
            return "null";
        }
        if (arg instanceof Number) {
            return arg.toString();
        }
        return JSONObject.toJSONString(arg);
    }
}
