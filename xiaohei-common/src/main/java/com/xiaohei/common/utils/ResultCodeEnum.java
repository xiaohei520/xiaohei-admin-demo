package com.xiaohei.common.utils;

import lombok.Getter;

/**
 * 统一返回结果状态信息类
 */
@Getter
public enum ResultCodeEnum {

    SUCCESS(200,"成功"),
    FAIL(201, "失败"),
    LOGIN_FAIL(240,"用户名与密码错误"),
    PARAM_ERROR( 202, "参数不正确"),
    SERVICE_ERROR(203, "服务异常"),
    DATA_ERROR(204, "数据异常"),
    DATA_UPDATE_ERROR(205, "数据版本异常"),

    LOGIN_AUTH(208, "未登陆"),
    PERMISSION(209, "没有权限"),



    CODE_ERROR(210, "验证码错误"),
//    LOGIN_MOBLE_ERROR(211, "账号不正确"),
    LOGIN_DISABLED_ERROR(212, "改用户已被禁用"),
    REGISTER_MOBLE_ERROR(213, "手机号已被使用"),
    LOGIN_AURH(214, "需要登录"),
    LOGIN_ACL(215, "没有权限"),



    URL_ENCODE_ERROR( 216, "URL编码失败"),
    ILLEGAL_CALLBACK_REQUEST_ERROR( 217, "非法回调请求"),
    FETCH_USERINFO_ERROR( 219, "获取用户信息失败"),
    PARAMS_ERROR(220,"请求数据转换异常"),
    PARAMS_NULL( 221, "请求参数为空"),
    QUARTZ_ERROR( 222, "服务已存在"),
    QUARTZ_CRON_ERROR( 223, "cron格式错误"),
    REQUEST_ERROR(224,"请务重复请求!!!"),
    VALIDATE_FAILED(225, "参数检验失败"),
    ;
    //LOGIN_ERROR( 23005, "登录失败"),



    private Integer code;
    private String message;

    private ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}