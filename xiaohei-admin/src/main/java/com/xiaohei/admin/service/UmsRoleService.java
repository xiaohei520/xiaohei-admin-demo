package com.xiaohei.admin.service;

import com.xiaohei.mbg.model.UmsMenu;
import com.xiaohei.mbg.model.UmsResource;
import com.xiaohei.mbg.model.UmsRole;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UmsRoleService {
    //创建
    int create(UmsRole role);

    //更新
    int update(Integer id, UmsRole role);

    //删除
    int delete(List<Integer> ids);

    //查询全部
    List<UmsRole> list();

    //分页
    List<UmsRole> list(String keyword, Integer pageSize, Integer pageNum);


    //通过角色id来查询可以访问的Menu
    List<UmsMenu> listMenu(Integer roleId);


    List<UmsResource> listResources(Integer roleId);

    //给角色分配菜单
    @Transactional
    int allocMenu(Integer roleId, List<Integer> menuIds);

    //给角色分配资源
    @Transactional
    int allocResource(Integer roleId, List<Integer> resourceIds);
}
