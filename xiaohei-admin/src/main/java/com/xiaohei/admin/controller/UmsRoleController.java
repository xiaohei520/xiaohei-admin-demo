package com.xiaohei.admin.controller;

import com.xiaohei.admin.dto.UmsMenuNode;
import com.xiaohei.admin.dto.UmsResourceNode;
import com.xiaohei.admin.service.UmsMenuService;
import com.xiaohei.admin.service.UmsResourceService;
import com.xiaohei.admin.service.UmsRoleService;
import com.xiaohei.common.api.CommonPage;
import com.xiaohei.common.utils.Result;
import com.xiaohei.mbg.model.UmsMenu;
import com.xiaohei.mbg.model.UmsResource;
import com.xiaohei.mbg.model.UmsRole;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName: UmsRoleController
 * @Description:
 * @Author: xiaohei on Date:2022/6/28 12:56
 * @Version: 1.0
 */
@RestController
@RequestMapping("/role")
public class UmsRoleController {

    @Autowired
    private UmsRoleService roleService;

    @Autowired
    private UmsMenuService menuService;

    @Autowired
    private UmsResourceService resourceService;

    @ApiOperation(value = "添加角色")
    @PostMapping(value = "/create")
    public Result create(@RequestBody UmsRole role) {
        int count = roleService.create(role);
        if (count > 0) {
            return Result.ok(count);
        }
        return Result.fail();
    }

    @ApiOperation(value = "修改角色")
    @PostMapping(value = "/update/{id}")
    public Result update(@PathVariable Integer id,@RequestBody UmsRole role) {
        int count = roleService.update(id, role);
        if (count > 0) {
            return Result.ok(count);
        }
        return Result.fail();
    }

    @ApiOperation(value = "批量删除角色")
    @PostMapping(value = "/delete")
    public Result delete(@RequestParam(value = "ids") List<Integer> ids) {
        int count = roleService.delete(ids);
        if (count > 0) {
            return Result.ok(count);
        }
        return Result.fail();
    }

    @ApiOperation(value = "获取所有角色")
    @GetMapping(value = "/listAll")
    public Result listAll() {
        List<UmsRole> roleList = roleService.list();
        return Result.ok(roleList);
    }

    @ApiOperation("根据角色名称分页获取角色列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Result<CommonPage<UmsRole>> list(@RequestParam(value = "keyword", required = false) String keyword,
                                                  @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        List<UmsRole> roleList = roleService.list(keyword, pageSize, pageNum);
        return Result.ok(CommonPage.restPage(roleList));
    }

    @ApiOperation(value = "修改角色状态")
    @PostMapping(value = "/updateStatus/{id}")
    public Result updateStatus(@PathVariable Integer id, @RequestParam(value = "status") Integer status) {
        UmsRole umsRole = new UmsRole();
        umsRole.setStatus(status);
        int count = roleService.update(id, umsRole);
        if (count > 0) {
            return Result.ok(count);
        }
        return Result.fail();
    }

    @ApiOperation(value = "获取角色相关菜单")
    @GetMapping(value = "/listMenu/{roleId}")
    public Result listMenu(@PathVariable Integer roleId) {
        List<UmsMenu> umsMenus = roleService.listMenu(roleId);
        umsMenus.removeAll(Collections.singleton(null));
        return Result.ok(umsMenus);
    }

    @ApiOperation(value = "获取角色相关资源")
    @GetMapping(value = "/listResource/{roleId}")
    public Result listResource(@PathVariable Integer roleId) {
        List<UmsResource> umsMenus = roleService.listResources(roleId);
        umsMenus.removeAll(Collections.singleton(null));
        return Result.ok(umsMenus);
    }

    @ApiOperation(value = "给角色分配菜单")
    @PostMapping(value = "/allocMenu")
    public Result allocMenu(@RequestParam Integer roleId, @RequestParam List<Integer> menuIds) {
        int count = roleService.allocMenu(roleId, menuIds);
        return Result.ok(count);
    }

    @ApiOperation(value = "给角色分配菜单")
    @PostMapping(value = "/allocResource")
    public Result allocResource(@RequestParam Integer roleId, @RequestParam List<Integer> resourceIds) {
        int count = roleService.allocResource(roleId, resourceIds);
        return Result.ok(count);
    }

    @ApiOperation(value = "获取树形的菜单列表")
    @GetMapping(value = "/treeMenuList")
    public Result treeMenuList() {
        List<UmsMenuNode> menuNode = menuService.treeList();
        return Result.ok(menuNode);
    }

    @ApiOperation(value = "获取树形的资源列表")
    @GetMapping(value = "/treeResourceList")
    public Result treeResourceList() {
        List<UmsResourceNode> resourceNodeList = resourceService.treeList();
        return Result.ok(resourceNodeList);
    }

}
