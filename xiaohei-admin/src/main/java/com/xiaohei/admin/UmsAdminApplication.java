package com.xiaohei.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName: UmsAdminApplication
 * @Description:
 * @Author: xiaohei on Date:2022/6/28 16:04
 * @Version: 1.0
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.xiaohei"})
public class UmsAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(UmsAdminApplication.class, args);
    }
}
