package com.xiaohei.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

public class UmsResource implements Serializable {
    @ApiModelProperty(value = "后台接口资源ID")
    private Integer resourceId;

    @ApiModelProperty(value = "后台接口需要的权限名称")
    private String name;

    @ApiModelProperty(value = "前端显示的权限类别")
    private String nameZh;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "后台的url接口, 使用ant来表达或绝对的api")
    private String url;

    @ApiModelProperty(value = "创建资源时间")
    private Date createTime;

    private Integer categoryId;

    @ApiModelProperty(value = "创建时间")
    private Date dCreateTime;

    @ApiModelProperty(value = "更新时间")
    private Date dUpdateTime;

    private static final long serialVersionUID = 1L;

    public Integer getResourceId() {
        return resourceId;
    }

    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameZh() {
        return nameZh;
    }

    public void setNameZh(String nameZh) {
        this.nameZh = nameZh;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Date getdCreateTime() {
        return dCreateTime;
    }

    public void setdCreateTime(Date dCreateTime) {
        this.dCreateTime = dCreateTime;
    }

    public Date getdUpdateTime() {
        return dUpdateTime;
    }

    public void setdUpdateTime(Date dUpdateTime) {
        this.dUpdateTime = dUpdateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", resourceId=").append(resourceId);
        sb.append(", name=").append(name);
        sb.append(", nameZh=").append(nameZh);
        sb.append(", description=").append(description);
        sb.append(", url=").append(url);
        sb.append(", createTime=").append(createTime);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", dCreateTime=").append(dCreateTime);
        sb.append(", dUpdateTime=").append(dUpdateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}