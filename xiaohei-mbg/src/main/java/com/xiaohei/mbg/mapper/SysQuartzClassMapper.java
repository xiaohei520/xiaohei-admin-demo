package com.xiaohei.mbg.mapper;

import com.xiaohei.mbg.model.SysQuartzClass;
import com.xiaohei.mbg.model.SysQuartzClassExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysQuartzClassMapper {
    long countByExample(SysQuartzClassExample example);

    int deleteByExample(SysQuartzClassExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SysQuartzClass record);

    int insertSelective(SysQuartzClass record);

    List<SysQuartzClass> selectByExample(SysQuartzClassExample example);

    SysQuartzClass selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SysQuartzClass record, @Param("example") SysQuartzClassExample example);

    int updateByExample(@Param("record") SysQuartzClass record, @Param("example") SysQuartzClassExample example);

    int updateByPrimaryKeySelective(SysQuartzClass record);

    int updateByPrimaryKey(SysQuartzClass record);
}