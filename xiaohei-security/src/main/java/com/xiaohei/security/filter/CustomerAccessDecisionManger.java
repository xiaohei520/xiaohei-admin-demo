package com.xiaohei.security.filter;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @ClassName: CustomerAccessDeciisionManger
 * @Description:
 * @Author: xiaohei on Date:2022/4/23 15:32
 * @Version: 1.0
 */
public class CustomerAccessDecisionManger implements AccessDecisionManager {

    /**
     * 判断当前登录的用户是否具备当前请求URL所需要的角色信息
     * 如果不具备，就抛出 AccessDeniedException 异常，否则不做任何事即可
     *
     * @param authentication 当前登录用户的信息
     * @param object         FilterInvocation对象，可以获取当前请求对象
     * @param configAttributes  FilterInvocationSecurityMetadataSource 中的 getAttributes() 方法的返回值，++-
     *                       即当前请求URL所需的角色
     */
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        // 当接口未被配置资源时直接放行
        if (configAttributes.size() == 0) {
            return;
        }

        //这个是当前用户的角色
        Collection<? extends GrantedAuthority> auths = authentication.getAuthorities();

        //这个是接口的权限
        for (ConfigAttribute configAttribute : configAttributes) {
            String attribute = configAttribute.getAttribute();
            for (GrantedAuthority auth : auths) {
                //这里判断只要这个用户有其中一个权限就代表他是可以访问这个接口的
                if (attribute.trim().equals(auth.getAuthority())){
                    return;
                }
            }
        }
        throw new AccessDeniedException("抱歉，您没有访问权限");
    }

    //是否支持校验
    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    //是否支持校验
    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
