

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `log_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `user_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作人id',
  `descriptio` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作的方法',
  `params` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '方法的参数',
  `ip` int(0) NULL DEFAULT NULL COMMENT '用户的ip',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` int(0) NULL DEFAULT NULL COMMENT '花费时间',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (1, 'user3', '登录', 'com.xiaohei.admin.controller.UmsAdminController.login()', ' loginParam:AdminLoginParam(username=user3, password=123456)', 0, '2022-08-08 21:39:39', 412);
INSERT INTO `sys_log` VALUES (2, 'user3', '登录', 'com.xiaohei.admin.controller.UmsAdminController.login()', ' loginParam:AdminLoginParam(username=user3, password=123456)', 0, '2022-08-09 15:47:51', 1465);

-- ----------------------------
-- Table structure for sys_quartz
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz`;
CREATE TABLE `sys_quartz`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `class_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务类名',
  `cron_expression` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `param` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `description` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '状态（0--正常1--停用）',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除状态（0，正常，1已删除）',
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_quartz
-- ----------------------------
INSERT INTO `sys_quartz` VALUES (23, 'com.xiaohei.quartz.job.TestJob', '* * * * * ?', '1', NULL, '2022-07-14 13:11:21', 'user3', 1, 1, NULL, '2022-07-16 21:22:53');

-- ----------------------------
-- Table structure for sys_quartz_class
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz_class`;
CREATE TABLE `sys_quartz_class`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `class_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务类名',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '定时任务名',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除状态（0，正常，1已删除）',
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 145 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_quartz_class
-- ----------------------------
INSERT INTO `sys_quartz_class` VALUES (144, 'com.xiaohei.quartz.job.TestJob', '删除数据库逻辑删除的row', 0, '2022-09-21 13:43:19', NULL);

-- ----------------------------
-- Table structure for ums_admin
-- ----------------------------
DROP TABLE IF EXISTS `ums_admin`;
CREATE TABLE `ums_admin`  (
  `admin_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '后台管理用户id',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` int(0) NULL DEFAULT 1 COMMENT '用户状态',
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`admin_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ums_admin
-- ----------------------------
INSERT INTO `ums_admin` VALUES (1, 'user3', '$2a$10$7CIALeHbg1/GQApb3u91GOTUtzimTtg924Xw/lEEJWfYHWFPSgL.W', 'wewae', '20320302@@@@', '小黑', '2022-06-20 18:58:14', '2022-06-20 18:58:14', 1, '2022-07-12 00:32:58', NULL);
INSERT INTO `ums_admin` VALUES (2, 'xiaohei', '$2a$10$7CIALeHbg1/GQApb3u91GOTUtzimTtg924Xw/lEEJWfYHWFPSgL.W', 'weiwei', '23213@QQ', 'xiaohei', '2022-06-25 16:31:36', '2022-06-25 16:31:39', 1, '2022-07-12 00:32:58', NULL);
INSERT INTO `ums_admin` VALUES (3, 'user2', '$2a$10$7CIALeHbg1/GQApb3u91GOTUtzimTtg924Xw/lEEJWfYHWFPSgL.W', 'wewae', '20320302@@@@', '小黑', '2022-06-25 23:16:35', '2022-06-25 23:16:35', 1, '2022-07-12 00:32:58', NULL);
INSERT INTO `ums_admin` VALUES (5, 'user7', '$2a$10$3kVXrupGc7mAl0pecLrN2.Hht5gq9ByHurrj4fHmUYFI/.dizr1n2', NULL, '231@32131.com', 'wqeqw', '2022-09-13 13:33:20', '2022-09-13 13:33:20', NULL, NULL, NULL);
INSERT INTO `ums_admin` VALUES (7, 'user9', '$2a$10$O8ZAm13CWzGsHY5lh4Bw9OkvJRX90q233O.otIn9LFVQCnoYTMzqW', NULL, '293231@22.com', 'asd6565', '2022-09-13 22:48:00', '2022-09-13 22:48:00', 1, NULL, '2022-09-20 13:40:04');
INSERT INTO `ums_admin` VALUES (8, 'user10', '$2a$10$yyNWBxntxOxNwGw66f/v8ewqSY30TYcX9yn5uP678WJHZUXLnEpda', NULL, 'xca@wq.com', '123', '2022-09-13 22:52:54', '2022-09-13 22:52:54', 1, NULL, NULL);
INSERT INTO `ums_admin` VALUES (9, 'user18', '$2a$10$R5IYK8Kqz8xvqDnm9J.Y0ekjGG3FKvAGxYMOf9itu0ObBtyhxUcbq', NULL, 'wead@11.com', '213', '2022-09-13 22:59:58', '2022-09-13 22:59:58', 1, NULL, NULL);
INSERT INTO `ums_admin` VALUES (11, 'user#', '$2a$10$gij36E3Xij1olKsflOPEKOavwrCmSmkiMG/pBpSuA4jzsNFtZFg4K', NULL, '123@31.com', '123', '2022-09-20 10:25:21', '2022-09-20 10:25:21', 1, NULL, NULL);
INSERT INTO `ums_admin` VALUES (12, 'user006', '$2a$10$A1N0h31ngHqVj6I/Hudg7.NVkpK5Gwl97sedCj2NsJpLXrvkcl.Ya', NULL, '1231@!312', '测试213', '2022-09-20 10:54:00', '2022-09-20 10:54:00', 1, NULL, '2022-09-20 11:15:04');

-- ----------------------------
-- Table structure for ums_menu
-- ----------------------------
DROP TABLE IF EXISTS `ums_menu`;
CREATE TABLE `ums_menu`  (
  `menu_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '目录ID',
  `parent_id` int(0) NULL DEFAULT 0 COMMENT '目录id为parent_id,没有父类为0',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '后台显示目录title',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标的类名',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '前端的路由url',
  `level` int(0) NULL DEFAULT 1 COMMENT '0 -> 1',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `status` int(0) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '前端路径表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ums_menu
-- ----------------------------
INSERT INTO `ums_menu` VALUES (1, 0, '权限管理', 'el-icon-unlock', 'user', 0, '2022-06-25 13:45:00', '2022-07-12 00:33:00', '2022-09-20 10:06:00', 1);
INSERT INTO `ums_menu` VALUES (2, 1, '角色列表', 'el-icon-s-custom', 'role', 1, '2022-06-25 13:46:40', '2022-07-12 00:33:05', '2022-09-20 10:06:05', 1);
INSERT INTO `ums_menu` VALUES (3, 1, '资源列表', 'el-icon-s-claim', 'resource', 1, '2022-06-25 13:47:13', '2022-07-12 00:33:05', '2022-09-20 10:06:06', 1);
INSERT INTO `ums_menu` VALUES (4, 1, '菜单列表', 'el-icon-s-fold', 'menu', 1, '2022-06-25 13:47:56', '2022-07-12 00:33:05', '2022-09-20 10:06:24', 1);
INSERT INTO `ums_menu` VALUES (5, 1, '用户列表', 'el-icon-user-solid', 'admin', 1, '2022-06-25 13:48:11', '2022-07-12 00:33:05', '2022-09-20 10:06:08', 1);
INSERT INTO `ums_menu` VALUES (27, 0, '23', '312', '312', 0, '2022-09-20 13:56:00', NULL, '2022-09-20 13:56:43', NULL);
INSERT INTO `ums_menu` VALUES (28, 27, '测试', '123', '213', 1, '2022-09-21 14:45:57', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ums_resource
-- ----------------------------
DROP TABLE IF EXISTS `ums_resource`;
CREATE TABLE `ums_resource`  (
  `resource_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '后台接口资源ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '后台接口需要的权限名称',
  `name_zh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '前端显示的权限类别',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '后台的url接口, 使用ant来表达或绝对的api',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建资源时间',
  `category_id` int(0) NULL DEFAULT NULL,
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`resource_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ums_resource
-- ----------------------------
INSERT INTO `ums_resource` VALUES (1, 'admin', '用户管理', '这是admin资源', '/admin/**', '2022-06-20 23:08:22', 1, '2022-07-12 00:33:05', '2022-07-13 17:20:52');
INSERT INTO `ums_resource` VALUES (3, 'menu', '菜单管理', '这是menu资源', '/menu/**', '2022-06-20 23:13:05', 1, '2022-07-12 00:33:05', '2022-07-13 17:20:52');
INSERT INTO `ums_resource` VALUES (14, 'resource', '资源管理', '这是resource资源', '/resource/**', '2022-06-28 15:36:34', 1, '2022-07-12 00:33:05', NULL);
INSERT INTO `ums_resource` VALUES (15, 'role', '角色管理', '这是role资源', '/role/**', '2022-06-28 15:37:11', 1, '2022-07-12 00:33:05', NULL);
INSERT INTO `ums_resource` VALUES (16, 'quartz', '定时任务', '这是quartz资源', '/quartz/**', '2022-07-12 13:16:00', 2, NULL, '2022-09-17 16:11:25');
INSERT INTO `ums_resource` VALUES (18, 'qutaz', '测试2', '定时任务的crud', '/qutaz/**', '2022-09-18 13:01:00', 2, NULL, NULL);
INSERT INTO `ums_resource` VALUES (19, 'xiaohei', '小黑buee', '测试', '/xiaohei/**', '2022-09-18 13:06:00', 2, NULL, NULL);

-- ----------------------------
-- Table structure for ums_resource_category
-- ----------------------------
DROP TABLE IF EXISTS `ums_resource_category`;
CREATE TABLE `ums_resource_category`  (
  `category_id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'category表id',
  `create_tiem` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用来表示后端路径的分类' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ums_resource_category
-- ----------------------------
INSERT INTO `ums_resource_category` VALUES (1, '2022-06-26 15:26:57', '权限', '2022-07-12 00:33:05', NULL);
INSERT INTO `ums_resource_category` VALUES (2, '2022-09-17 16:11:09', '定时任务', '2022-09-17 16:11:06', '2022-09-17 16:11:13');

-- ----------------------------
-- Table structure for ums_role
-- ----------------------------
DROP TABLE IF EXISTS `ums_role`;
CREATE TABLE `ums_role`  (
  `role_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色的名称, 名称不要紧, 逻辑开展',
  `status` int(0) NULL DEFAULT 1 COMMENT '1是启用, 0不启用',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '角色创建时间',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色权限的描叙',
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ums_role
-- ----------------------------
INSERT INTO `ums_role` VALUES (1, '超级管理员', 1, '2022-06-25 13:49:39', '只能....也只能..', '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role` VALUES (2, '测试', 1, '2022-06-25 13:59:22', '没有能力', '2022-07-12 00:33:06', NULL);

-- ----------------------------
-- Table structure for ums_role_admin_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_role_admin_relation`;
CREATE TABLE `ums_role_admin_relation`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'role_admin表ID',
  `role_id` int(0) NULL DEFAULT NULL COMMENT 'role表id',
  `admin_id` int(0) NULL DEFAULT NULL COMMENT 'admin表id',
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '前端路径表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ums_role_admin_relation
-- ----------------------------
INSERT INTO `ums_role_admin_relation` VALUES (1, 1, 1, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_admin_relation` VALUES (2, 2, 1, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_admin_relation` VALUES (3, 2, 2, '2022-07-12 00:33:06', NULL);

-- ----------------------------
-- Table structure for ums_role_menu_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_role_menu_relation`;
CREATE TABLE `ums_role_menu_relation`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'role_admin表ID',
  `role_id` int(0) NULL DEFAULT NULL COMMENT 'role表id',
  `menu_id` int(0) NULL DEFAULT NULL COMMENT 'admin表id',
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和全端页面' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ums_role_menu_relation
-- ----------------------------
INSERT INTO `ums_role_menu_relation` VALUES (5, 2, 1, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_menu_relation` VALUES (6, 2, 2, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_menu_relation` VALUES (7, 2, 3, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_menu_relation` VALUES (29, 1, 1, NULL, NULL);
INSERT INTO `ums_role_menu_relation` VALUES (30, 1, 2, NULL, NULL);
INSERT INTO `ums_role_menu_relation` VALUES (31, 1, 3, NULL, NULL);
INSERT INTO `ums_role_menu_relation` VALUES (32, 1, 4, NULL, NULL);
INSERT INTO `ums_role_menu_relation` VALUES (33, 1, 5, NULL, NULL);

-- ----------------------------
-- Table structure for ums_role_resource_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_role_resource_relation`;
CREATE TABLE `ums_role_resource_relation`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'role_admin表ID',
  `role_id` int(0) NULL DEFAULT NULL COMMENT 'role表id',
  `resource_id` int(0) NULL DEFAULT NULL COMMENT 'admin表id',
  `d_create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `d_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和资源中间表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ums_role_resource_relation
-- ----------------------------
INSERT INTO `ums_role_resource_relation` VALUES (1, 1, 1, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_resource_relation` VALUES (2, 1, 3, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_resource_relation` VALUES (3, 2, 3, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_resource_relation` VALUES (6, 1, 14, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_resource_relation` VALUES (7, 1, 15, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_resource_relation` VALUES (8, 2, 1, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_resource_relation` VALUES (9, 2, 15, '2022-07-12 00:33:06', NULL);
INSERT INTO `ums_role_resource_relation` VALUES (10, 1, 16, '2022-07-12 16:00:59', NULL);

SET FOREIGN_KEY_CHECKS = 1;
