package com.xiaohei.admin.config;

import com.xiaohei.common.config.BaseSwaggerConfig;
import com.xiaohei.common.domain.SwaggerProperties;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ClassName: MySwaggerConfig
 * @Description:
 * @Author: xiaohei on Date:2022/6/29 0:16
 * @Version: 1.0
 */
@Configuration
@EnableSwagger2
public class MySwaggerConfig extends BaseSwaggerConfig {

    @Override
    public SwaggerProperties swaggerProperties() {
        return SwaggerProperties.builder()
                .apiBasePackage("com.xiaohei.admin.controller")
                .title("xiaohei后台系统")
                .description("xiaohei后台相关接口文档")
                .contactName("xiaohei")
                .version("1.0")
                .enableSecurity(true)
                .build();
    }
}
