package com.xiaohei.admin.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

/**
 * @ClassName: AdminLoginParam
 * @Description:
 * @Author: xiaohei on Date:2022/6/25 17:09
 * @Version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AdminLoginParam {
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;
}
