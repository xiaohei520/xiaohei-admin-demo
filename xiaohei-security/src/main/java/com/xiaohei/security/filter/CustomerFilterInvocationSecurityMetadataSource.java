package com.xiaohei.security.filter;

import com.xiaohei.security.common.DynamicSecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.util.AntPathMatcher;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * @ClassName: CustomerFilterInvocationSecurityMetadataSource
 * @Description:
 * @Author: xiaohei on Date:2022/4/23 15:16z
 * @Version: 1.0
 */
public class CustomerFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    @Autowired
    private DynamicSecurityService dynamicSecurityService;
    private static final Logger log = LoggerFactory.getLogger(CustomerFilterInvocationSecurityMetadataSource.class);
    private static Map<String,ConfigAttribute> configAttributeMap = null;

    /**
     * 获取所有的资源信息
     * <p>
     * 在getAttributes()方法内不可直接使用menuMapper，因为Security先于Spring加载，此时还没有注入，会报空指针异常
     * 被@PostConstruct修饰的方法会在服务器加载Servlet的时候运行，并且只会被服务器执行一次,依赖注入初始化后会自动执行该方法
     */

    @PostConstruct
    public void loadDataSource(){
        configAttributeMap = dynamicSecurityService.loadDataSource();
        log.info("接口的所有权限: {}",configAttributeMap);
    }

    public void clearDataSources(){
        configAttributeMap.clear();
        configAttributeMap = null;
        log.info("所有接口的权限信息清空完毕 ! !");
    }

    //getAttributes这个在被SpringSecurity管理
    //每次请求有权限认证的接口时,就会调用这个方法

    /**
     *这个方法具体做了什么捏?
     *      当前端请求一个url这个方法就会启动,通过FilterInvocation.getRequestUrl()获取到请求的URL地址
     *      在通过这个URL和数据库里面的路径进行匹配
     *      如果匹配成功,就给这个URL进行授权
     *      也就是这个URL要什么角色或者什么权限才能进行访问
     */

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        List<ConfigAttribute> configAttributes = new ArrayList<>();
        String url = ((FilterInvocation)object).getRequestUrl();

        Iterator<String> iterator = configAttributeMap.keySet().iterator();
        while (iterator.hasNext()) {
            String pattern = iterator.next();
            if (antPathMatcher.match(pattern,url)){
                configAttributes.add(configAttributeMap.get(pattern));
            }
        }
        return configAttributes;

    }

    //返回所有定义好的权限资源
    //Spring Security 在启动时会校验相关配置是否正确,如果不需要校验,直接返回null
    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
