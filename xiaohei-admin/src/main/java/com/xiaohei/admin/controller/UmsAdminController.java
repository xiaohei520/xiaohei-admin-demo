package com.xiaohei.admin.controller;

import cn.hutool.core.collection.CollUtil;
import com.xiaohei.admin.dto.AdminLoginParam;
import com.xiaohei.admin.dto.AdminRegisterParam;
import com.xiaohei.admin.dto.UpdateAdminPasswordParam;
import com.xiaohei.admin.service.UmsAdminService;
import com.xiaohei.admin.service.UmsRoleService;
import com.xiaohei.common.annotation.LogAnnotation;
import com.xiaohei.common.annotation.PreventDuplication;
import com.xiaohei.common.api.AbstractCtrl;
import com.xiaohei.common.api.CommonPage;
import com.xiaohei.common.utils.Result;
import com.xiaohei.mbg.model.UmsAdmin;
import com.xiaohei.mbg.model.UmsRole;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: UmsAdminController
 * @Description:
 * @Author: xiaohei on Date:2022/6/27 16:28
 * @Version: 1.0
 */
@RestController
@RequestMapping("/admin")
public class UmsAdminController extends AbstractCtrl {
    @Autowired
    private UmsAdminService adminService;

    @Autowired
    private UmsRoleService roleService;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @ApiOperation(value = "用户注册")
    @PostMapping(value = "/register")
    public Result register(@Validated @RequestBody AdminRegisterParam adminRegisterParam) {
        UmsAdmin register = adminService.register(adminRegisterParam);
        if (register == null) {
            return Result.fail("注册失败! 用户已存在");
        }
        return Result.ok("注册成功");
    }

    @ApiOperation(value = "登录,返回token")
    @PostMapping(value = "/login")
    @LogAnnotation("登录")
    @PreventDuplication(expireSeconds = 5L)
    public Result login(@Validated @RequestBody AdminLoginParam loginParam) {
        String token = adminService.login(loginParam);
        if (token == null || token.equals("")) {
            return Result.fail("用户名或密码错误");
        }
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        tokenMap.put("tokenHeader", tokenHeader);
        tokenMap.put("tokenHead", tokenHead);
        return Result.ok(tokenMap);
    }

    @ApiOperation(value = "获取当前登录用户信息")
    @GetMapping(value = "/info")
    public Result getAdminInfo(Principal principal) {
        if (principal == null) {
            return Result.fail("获取用户信息失败");
        }
        String username = principal.getName();
        UmsAdmin admin = adminService.getAdminByUsername(username);
        Map<String, Object> data = new HashMap<>();
        data.put("username", admin.getUsername());
        data.put("userId", admin.getAdminId());
        data.put("menus", adminService.getMenuList(admin.getAdminId()));
        data.put("icon", admin.getIcon());
        List<UmsRole> roleList = adminService.getRoleList(admin.getAdminId());
        if (CollUtil.isNotEmpty(roleList)) {
            data.put("roles", roleList);
        }
        return Result.ok(data);

    }

    @ApiOperation("根据用户名或姓名分页获取用户列表")
    @GetMapping(value = "/list")
    public Result list(@RequestParam(value = "keyword", required = false) String keyword,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        List<UmsAdmin> adminList = adminService.list(keyword, pageSize, pageNum);
        return Result.ok(CommonPage.restPage(adminList));
    }

    @ApiOperation(value = "获取指定用户信息")
    @GetMapping(value = "/{id}")
    public Result getItem(@PathVariable Integer id) {
        UmsAdmin item = adminService.getItem(id);
        return Result.ok(item);
    }

    @ApiOperation(value = "修改指定用户信息")
    @PostMapping(value = "/update/{id}")
    public Result update(@PathVariable Integer id, @RequestBody UmsAdmin admin) {
        int count = adminService.update(id, admin);
        if (count > 0) {
            return Result.ok(count);
        }
        return Result.fail();
    }

    @ApiOperation(value = "修改指定用户密码")
    @PostMapping(value = "/updatePassword")
    public Result updatePassword(@Validated @RequestBody UpdateAdminPasswordParam updateAdminPasswordParam) {
        int status = adminService.updatePassword(updateAdminPasswordParam);
        switch (status) {
            case -1:
                return Result.fail("提交参数不合法");
            case -2:
                return Result.fail("找不到该用户");
            case -3:
                return Result.fail("旧密码错误");
            default:
                return Result.ok("修改成功");
        }
    }

    @ApiOperation(value = "删除指定用户信息")
    @GetMapping(value = "/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        int count = adminService.delete(id);
        if (count > 0) {
            return Result.ok("删除成功");
        }
        return Result.fail("删除失败");
    }

    @ApiOperation(value = "修改账号状态")
    @PostMapping(value = "/updateStatus/{id}")
    public Result updateStatus(@PathVariable Integer id, @RequestParam(value = "status") Integer status) {
        UmsAdmin admin = new UmsAdmin();
        admin.setStatus(status);
        int count = adminService.update(id, admin);
        if (count > 0) {
            return Result.ok();
        }
        return Result.fail();
    }

    @ApiOperation("给用户分配角色")
    @PostMapping(value = "/role/update")
    public Result updateRole(@RequestParam("adminId") Integer adminId,
                                   @RequestParam("roleIds") List<Integer> roleIds) {
        int count = adminService.updateRole(adminId, roleIds);
        if (count >= 0) {
            return Result.ok(count);
        }
        return Result.fail();
    }

    @ApiOperation("获取指定用户的角色")
    @GetMapping(value = "/role/{adminId}")
    public Result<List<UmsRole>> getRoleList(@PathVariable Integer adminId) {
        List<UmsRole> roleList = adminService.getRoleList(adminId);
        return Result.ok(roleList);
    }

    @ApiOperation(value = "获取所以的角色信息")
    @GetMapping(value = "/role/listAll")
    public Result getListAll(){
        List<UmsRole> list = roleService.list();
        return Result.ok(list);
    }
}
