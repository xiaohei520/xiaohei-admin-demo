package com.xiaohei.quartz.service;

import com.xiaohei.mbg.model.SysQuartzClass;

import java.util.List;

/**
 * @author 凌殇
 * @ClassName: SysQuartzClassService
 * @Description:
 * @date 2022/07/13 16:50
 * @Version: 1.0
 */

public interface SysQuartzClassService {

    List<SysQuartzClass> select(String name);

    //查询全部
    List<SysQuartzClass> selectAll();

    //模糊查询
    List<SysQuartzClass> selectByFuzzy(String name);
}
