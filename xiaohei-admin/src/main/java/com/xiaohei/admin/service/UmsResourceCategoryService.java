package com.xiaohei.admin.service;

import com.xiaohei.mbg.model.UmsResourceCategory;

import java.util.List;

public interface UmsResourceCategoryService {

    //获取所有资源分类
    List<UmsResourceCategory> listAll();

    //创建资源分类
    int create(UmsResourceCategory resourceCategory);

    //修改资源分类
    int update(Integer id, UmsResourceCategory resourceCategory);

    //删除资源分类
    int delete(Integer id);
}
