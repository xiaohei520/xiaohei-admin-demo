package com.xiaohei.quartz.dao;

public interface SysQuartzDao {
    int updateStatus(String className, Boolean status);
}
