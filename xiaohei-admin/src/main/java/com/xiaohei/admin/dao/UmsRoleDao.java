package com.xiaohei.admin.dao;

import com.xiaohei.mbg.model.UmsMenu;
import com.xiaohei.mbg.model.UmsResource;

import java.util.List;

public interface UmsRoleDao {

    //根据角色id来查询用户所有菜单权限
    List<UmsMenu> getMenuListByRoleId(Integer roleId);

    //根据角色id来查询用户所有资源权限
    List<UmsResource> getResourceListByRoleId(Integer roleId);


}
