package com.xiaohei.quartz.service.impl;

import com.xiaohei.common.utils.IsNullUtils;
import com.xiaohei.mbg.mapper.SysQuartzClassMapper;
import com.xiaohei.mbg.model.SysQuartzClass;
import com.xiaohei.mbg.model.SysQuartzClassExample;
import com.xiaohei.quartz.service.SysQuartzClassService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 凌殇
 * @ClassName: SysQuartzClassServiceImpl
 * @Description:
 * @date 2022/07/13 16:51
 * @Version: 1.0
 */
@Service
public class SysQuartzClassServiceImpl implements SysQuartzClassService {

    private static final Logger log = LoggerFactory.getLogger(SysQuartzServiceImpl.class);

    @Autowired
    SysQuartzClassMapper sysQuartzClassMapper;

    @Override
    public List<SysQuartzClass> select(String name) {
        SysQuartzClassExample example = new SysQuartzClassExample();
        example.createCriteria().andNameEqualTo(name);
        example.createCriteria().andDelFlagEqualTo(false);
        return sysQuartzClassMapper.selectByExample(example);

    }

    @Override
    public List<SysQuartzClass> selectAll() {
        SysQuartzClassExample example = new SysQuartzClassExample();
        example.createCriteria().andDelFlagEqualTo(false);
        return sysQuartzClassMapper.selectByExample(example);
    }

    @Override
    public List<SysQuartzClass> selectByFuzzy(String name) {
        SysQuartzClassExample example = new SysQuartzClassExample();
        if (!IsNullUtils.isNull(name)){
            example.createCriteria().andNameLike("%" + name + "%");
        }
        example.createCriteria().andDelFlagEqualTo(false);
        return sysQuartzClassMapper.selectByExample(example);
    }
}
