package com.xiaohei.admin.service.impl;

import com.xiaohei.admin.service.UmsResourceCategoryService;
import com.xiaohei.mbg.mapper.UmsResourceCategoryMapper;
import com.xiaohei.mbg.model.UmsResourceCategory;
import com.xiaohei.mbg.model.UmsResourceCategoryExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: UmsResourceCategoryServiceImpl
 * @Description:
 * @Author: xiaohei on Date:2022/6/28 15:25
 * @Version: 1.0
 */
@Service
public class UmsResourceCategoryServiceImpl implements UmsResourceCategoryService {

    @Autowired
    private UmsResourceCategoryMapper resourceCategoryMapper;

    @Override
    public List<UmsResourceCategory> listAll() {

        List<UmsResourceCategory> resourceCategoryList = resourceCategoryMapper.selectByExample(new UmsResourceCategoryExample());
        return resourceCategoryList;
    }

    @Override
    public int create(UmsResourceCategory resourceCategory) {
        resourceCategory.setCreateTiem(new Date());
        int insert = resourceCategoryMapper.insert(resourceCategory);
        return insert;
    }

    @Override
    public int update(Integer id, UmsResourceCategory resourceCategory) {
        resourceCategory.setCategoryId(id);
        int update = resourceCategoryMapper.updateByPrimaryKeySelective(resourceCategory);
        return update;
    }

    @Override
    public int delete(Integer id) {
        return resourceCategoryMapper.deleteByPrimaryKey(id);
    }
}
