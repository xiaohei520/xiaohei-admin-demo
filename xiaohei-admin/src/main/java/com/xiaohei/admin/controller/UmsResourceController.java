package com.xiaohei.admin.controller;

import com.xiaohei.admin.service.UmsResourceService;
import com.xiaohei.common.api.CommonPage;
import com.xiaohei.common.utils.Result;
import com.xiaohei.mbg.model.UmsResource;
import com.xiaohei.security.filter.CustomerFilterInvocationSecurityMetadataSource;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName: UmsResourceController
 * @Description:
 * @Author: xiaohei on Date:2022/6/28 13:26
 * @Version: 1.0
 */
@RestController
@RequestMapping("/resource")
public class UmsResourceController {

    @Autowired
    private UmsResourceService resourceService;

    @Autowired
    private CustomerFilterInvocationSecurityMetadataSource securityMetadataSource;

    @ApiOperation(value = "添加后台资源")
    @PostMapping(value = "/create")
    public Result create(@RequestBody UmsResource resource) {
        int count = resourceService.create(resource);
        if (count > 0) {
            securityMetadataSource.loadDataSource();
            return Result.ok(count);
        }
        return Result.fail();
    }

    @ApiOperation(value = "修改后台资源")
    @PostMapping(value = "/update/{id}")
    public Result update(@PathVariable Integer id, @RequestBody UmsResource resource) {
        int count = resourceService.update(id, resource);
        if (count > 0) {
            securityMetadataSource.loadDataSource();
            return Result.ok(count);
        }
        return Result.fail();
    }

    @ApiOperation(value = "根据ID获取资源详细")
    @GetMapping(value = "/{id}")
    public Result getItem(@PathVariable Integer id) {
        UmsResource item = resourceService.getItem(id);
        if (item != null) {
            return Result.ok(item);
        }
        return Result.fail("无这个id的用户");
    }

    @ApiOperation(value = "根据资源id删除")
    @GetMapping(value = "/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        int count = resourceService.delete(id);
        securityMetadataSource.loadDataSource();
        if (count > 0) {
            return Result.ok();
        }
        return Result.fail();
    }

    @ApiOperation("分页模糊查询后台资源")
    @GetMapping(value = "/list")
    public Result<CommonPage<UmsResource>> list(@RequestParam(required = false) String nameKeyword,
                                                @RequestParam(required = false) String urlKeyword,
                                                @RequestParam(required = false) Integer categoryId,
                                                @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        List<UmsResource> resourceList = resourceService.list(nameKeyword, urlKeyword,categoryId, pageSize, pageNum);
        return Result.ok(CommonPage.restPage(resourceList));
    }

    @ApiOperation(value = "查询所有后台资源")
    @GetMapping(value = "/listAll")
    public Result listAll() {
        List<UmsResource> resourceList = resourceService.listAll();
        return Result.ok(resourceList);
    }

}
