package com.xiaohei.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 防止重复操作注解
 */
@Target(ElementType.METHOD) //在方法上使用
@Retention(RetentionPolicy.RUNTIME) //在运行是保存信息
public @interface PreventDuplication {

    /**
     * 存储redis限时标记数值
     * @return
     */
    String value() default "value";

    /**
     * 过时时间
     * @return
     */
    long expireSeconds() default 10;

}
