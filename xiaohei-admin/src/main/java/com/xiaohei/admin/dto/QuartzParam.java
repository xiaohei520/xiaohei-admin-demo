package com.xiaohei.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * @ClassName: QuartzParam
 * @Description:
 * @Author: xiaohei on Date:2022/7/11 17:37
 * @Version: 1.0
 */
@Getter
@Setter
public class QuartzParam {

    @ApiModelProperty("任务类名")
    @NotEmpty
    private String className;

    @ApiModelProperty("cron表达式")
    @NotEmpty
    private String cronExpression;

    @ApiModelProperty("参数")
    private String param;

    @ApiModelProperty("描述")
    @NotEmpty
    private String descript;

    @ApiModelProperty("任务状态")
    @NotEmpty
    private Boolean status;

}
