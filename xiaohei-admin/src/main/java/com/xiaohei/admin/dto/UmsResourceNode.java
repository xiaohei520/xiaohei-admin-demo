package com.xiaohei.admin.dto;

import com.xiaohei.mbg.model.UmsResource;
import com.xiaohei.mbg.model.UmsResourceCategory;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @ClassName: UmsResourceNode
 * @Description:
 * @Author: xiaohei on Date:2022/6/26 15:15
 * @Version: 1.0
 */
@Getter
@Setter
public class UmsResourceNode extends UmsResourceCategory {
    private List<UmsResource> children;
}
