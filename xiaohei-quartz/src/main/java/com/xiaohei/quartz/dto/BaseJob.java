package com.xiaohei.quartz.dto;

import org.quartz.Job;

/**
 * @author 凌殇
 * @date 2022年07月13日 15:11
 */

public interface BaseJob extends Job {

    String getName();

}
