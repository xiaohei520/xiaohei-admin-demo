package com.xiaohei.quartz.job;

import com.xiaohei.quartz.dto.BaseJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestJob implements BaseJob {

    private static final Logger log = LoggerFactory.getLogger(TestJob.class);

    private String name = "删除数据库逻辑删除的row";

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.warn("定时任务启动");
    }

    @Override
    public String getName() {
        return name;
    }
}
