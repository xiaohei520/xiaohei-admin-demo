package com.xiaohei.mbg.mapper;

import com.xiaohei.mbg.model.UmsRoleAdminRelation;
import com.xiaohei.mbg.model.UmsRoleAdminRelationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsRoleAdminRelationMapper {
    long countByExample(UmsRoleAdminRelationExample example);

    int deleteByExample(UmsRoleAdminRelationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UmsRoleAdminRelation record);

    int insertSelective(UmsRoleAdminRelation record);

    List<UmsRoleAdminRelation> selectByExample(UmsRoleAdminRelationExample example);

    UmsRoleAdminRelation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UmsRoleAdminRelation record, @Param("example") UmsRoleAdminRelationExample example);

    int updateByExample(@Param("record") UmsRoleAdminRelation record, @Param("example") UmsRoleAdminRelationExample example);

    int updateByPrimaryKeySelective(UmsRoleAdminRelation record);

    int updateByPrimaryKey(UmsRoleAdminRelation record);
}