package com.xiaohei.admin.config;

import com.xiaohei.common.config.BaseRedisConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: MyRedisConfig
 * @Description:
 * @Author: xiaohei on Date:2022/8/9 14:58
 * @Version: 1.0
 */
@Configuration
public class MyRedisConfig extends BaseRedisConfig {
}
