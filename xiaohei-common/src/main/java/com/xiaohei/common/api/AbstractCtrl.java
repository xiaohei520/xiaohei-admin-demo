package com.xiaohei.common.api;

import com.alibaba.fastjson.JSONObject;
import com.xiaohei.common.exception.ApiException;
import com.xiaohei.common.utils.ResultCodeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

/**
 * @ClassName: AbstractCtrl
 * @Description:
 * @Author: xiaohei on Date:2022/7/10 22:30
 * @Version: 1.0
 */
public class AbstractCtrl {

    protected static final Logger logger = LoggerFactory.getLogger(AbstractCtrl.class);

    private static final String PAGE_INDEX_PARAM_NAME = "pageNum";  //分页页码 参数名
    private static final String PAGE_SIZE_PARAM_NAME = "pageSize";  //分页条数 参数名
    private static final int DEFAULT_PAGE_INDEX = 1;  // 默认页码： 第一页
    private static final int DEFAULT_PAGE_SIZE = 20;  // 默认条数： 20

    private static final String SORT_FIELD_PARAM_NAME = "sortField";  //排序字段
    private static final String SORT_ORDER_FLAG_PARAM_NAME = "sortOrder";  // 排序正序， 倒序标志

    @Autowired
    protected HttpServletRequest request;   //自动注入request

    @Autowired
    protected HttpServletResponse response;  //自动注入response

    @Autowired
    protected RequestKitBean requestKitBean;

    /** 获取json格式的请求参数 **/
    protected JSONObject getReqParamJSON(){
        return requestKitBean.getReqParamJSON();
    }

    /** 获取页码 **/
    protected int getPageIndex() {
        Integer pageIndex = getReqParamJSON().getInteger(PAGE_INDEX_PARAM_NAME);
        if(pageIndex == null) {
            return DEFAULT_PAGE_INDEX;
        }
        return pageIndex;
    }

    /** 获取条数， 默认不允许查询全部数据 **/
    protected int getPageSize() {
        return getPageSize(false);
    }

    /** 获取条数,  加入条件：是否允许获取全部数据 **/
    protected int getPageSize(boolean allowQueryAll) {
        Integer pageSize = getReqParamJSON().getInteger(PAGE_SIZE_PARAM_NAME);

        if(allowQueryAll && pageSize != null && pageSize == -1) {
            return Integer.MAX_VALUE; // -1代表获取全部数据，查询int最大值的数据
        }
        if(pageSize == null || pageSize < 0) {
            return DEFAULT_PAGE_SIZE;
        }
        return pageSize;
    }

    ///** 获取Ipage分页信息, 默认不允许获取全部数据 **/
    //protected IPage getIPage(){
    //    return (IPage) new Page(getPageIndex(), getPageSize());
    //}
    //
    ///** 获取Ipage分页信息, 加入条件：是否允许获取全部数据 **/
    //protected IPage getIPage(boolean allowQueryAll){
    //    return (IPage) new Page(getPageIndex(), getPageSize(allowQueryAll));
    //}

    /** 获取请求参数值 [ T 类型 ], [ 非必填 ] **/
    protected <T> T getVal(String key, Class<T> cls) {
        return getReqParamJSON().getObject(key, cls);
    }

    /** 获取请求参数值 [ T 类型 ], [ 必填 ] **/
    protected <T> T getValRequired(String key, Class<T> cls) {
        T value = getVal(key, cls);
        if(ObjectUtils.isEmpty(value)) {
            throw new ApiException(ResultCodeEnum.PARAMS_NULL);
        }
        return value;
    }

    /** 获取请求参数值 [ T 类型 ], [ 如为null返回默认值 ] **/
    protected  <T> T getValDefault(String key, T defaultValue, Class<T> cls) {
        T value = getVal(key, cls);
        if(value == null) {
            return defaultValue;
        }
        return value;
    }

    /** 获取请求参数值 String 类型相关函数 **/
    protected String getValString(String key) {
        return getVal(key, String.class);
    }
    protected String getValStringRequired(String key) {
        return getValRequired(key, String.class);
    }
    protected String getValStringDefault(String key, String defaultValue) {
        return getValDefault(key, defaultValue, String.class);
    }

    /** 获取请求参数值 Byte 类型相关函数 **/
    protected Byte getValByte(String key) {
        return getVal(key, Byte.class);
    }
    protected Byte getValByteRequired(String key) {
        return getValRequired(key, Byte.class);
    }
    protected Byte getValByteDefault(String key, Byte defaultValue) {
        return getValDefault(key, defaultValue, Byte.class);
    }

    /** 获取请求参数值 Integer 类型相关函数 **/
    protected Integer getValInteger(String key) {
        return getVal(key, Integer.class);
    }
    protected Integer getValIntegerRequired(String key) {
        return getValRequired(key, Integer.class);
    }
    protected Integer getValIntegerDefault(String key, Integer defaultValue) {
        return getValDefault(key, defaultValue, Integer.class);
    }

    /** 获取请求参数值 Long 类型相关函数 **/
    protected Long getValLong(String key) {
        return getVal(key, Long.class);
    }
    protected Long getValLongRequired(String key) {
        return getValRequired(key, Long.class);
    }
    protected Long getValLongDefault(String key, Long defaultValue) {
        return getValDefault(key, defaultValue, Long.class);
    }

    /** 获取请求参数值 BigDecimal 类型相关函数 **/
    protected BigDecimal getValBigDecimal(String key) {
        return getVal(key, BigDecimal.class);
    }
    protected BigDecimal getValBigDecimalRequired(String key) {
        return getValRequired(key, BigDecimal.class);
    }
    protected BigDecimal getValBigDecimalDefault(String key, BigDecimal defaultValue) {
        return getValDefault(key, defaultValue, BigDecimal.class);
    }

    /** 获取对象类型 **/
    protected <T> T getObject(Class<T> clazz) {
        JSONObject params = getReqParamJSON();
        T result = params.toJavaObject(clazz);
        return result;
    }

    /** 校验参数值不能为空 */
    protected void checkRequired(String... keys) {
        for(String key : keys) {
            String value = getReqParamJSON().getString(key);
            if(StringUtils.isEmpty(value)) {
                throw new ApiException(ResultCodeEnum.PARAMS_NULL);
            }
        }
    }


    /** 获取客户端ip地址 **/
    public String getClientIp() {
        return requestKitBean.getClientIp();
    }


}
