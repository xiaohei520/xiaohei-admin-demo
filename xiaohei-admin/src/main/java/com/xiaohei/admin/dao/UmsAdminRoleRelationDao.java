package com.xiaohei.admin.dao;

import com.xiaohei.mbg.model.UmsMenu;
import com.xiaohei.mbg.model.UmsResource;
import com.xiaohei.mbg.model.UmsRole;
import com.xiaohei.mbg.model.UmsRoleAdminRelation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsAdminRoleRelationDao {
    //获取改用户的全部角色
    List<UmsRole> getRoleByAdminIdList(@Param("adminId") Integer adminId);

    //根据用户id 获取他拥有的菜单权限
    List<UmsMenu> getMenuByAdminIdList(@Param("adminId")  Integer adminId);

    //根据用户id 获取他拥有的资源权限
    List<UmsResource> getResourceByAdminIdList(@Param("adminId") Integer adminId);

    int insertList(@Param("list") List<UmsRoleAdminRelation> adminRoleRelationList);
}
