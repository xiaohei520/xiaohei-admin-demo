package com.xiaohei.admin.controller;

import com.xiaohei.admin.service.UmsMenuService;
import com.xiaohei.common.api.CommonPage;
import com.xiaohei.common.utils.Result;
import com.xiaohei.mbg.model.UmsMenu;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName: UmsMenuCtroller
 * @Description:
 * @Author: xiaohei on Date:2022/6/28 14:54
 * @Version: 1.0
 */
@RestController
@RequestMapping("/menu")
public class UmsMenuController {
    @Autowired
    private UmsMenuService menuService;

    @ApiOperation(value = "添加后台菜单")
    @PostMapping(value = "/create")
    public Result create( @RequestBody UmsMenu menu) {
        int count = menuService.create(menu);
        if (count > 0) {
            return Result.ok(count);
        }
        return Result.fail();
    }

    @ApiOperation("修改后台菜单")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public Result update(@PathVariable Integer id,
                               @RequestBody UmsMenu umsMenu) {
        int count = menuService.update(id, umsMenu);
        if (count > 0) {
            return Result.ok(count);
        } else {
            return Result.fail();
        }
    }

    @ApiOperation("根据ID获取菜单详情")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result<UmsMenu> getItem(@PathVariable Integer id) {
        UmsMenu umsMenu = menuService.getItem(id);
        return Result.ok(umsMenu);
    }

    @ApiOperation("根据ID删除后台菜单")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public Result delete(@PathVariable Integer id) {
        int count = menuService.delete(id);
        if (count > 0) {
            return Result.ok(count);
        } else {
            return Result.fail();
        }
    }

    @ApiOperation("分页查询后台菜单")
    @RequestMapping(value = "/list/{parentId}", method = RequestMethod.GET)
    public Result<CommonPage<UmsMenu>> list(@PathVariable Integer parentId,
                                                  @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        List<UmsMenu> menuList = menuService.list(parentId, pageSize, pageNum);
        return Result.ok(CommonPage.restPage(menuList));
    }
    
}
