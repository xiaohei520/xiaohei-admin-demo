package com.xiaohei.security.exception;

import cn.hutool.json.JSONUtil;
import com.xiaohei.common.utils.Result;
import com.xiaohei.common.utils.ResultCodeEnum;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: RestAuthenticationEntryPoint
 * @Description: 自定义返回结果, 为登录 或者 登录过期
 * @Author: xiaohei on Date:2022/6/20 15:47
 * @Version: 1.0
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JSONUtil.parse(Result.build("没有登录, 或者登录凭证过期,请重新登录", ResultCodeEnum.LOGIN_AURH)));
        response.getWriter().flush();
    }
}
