package com.xiaohei.admin.aspect;

import com.xiaohei.common.annotation.LogAnnotation;
import com.xiaohei.common.api.AbstractCtrl;
import com.xiaohei.common.exception.ApiException;
import com.xiaohei.common.utils.ResultCodeEnum;
import com.xiaohei.common.utils.StringAddressToUtils;
import com.xiaohei.mbg.mapper.SysLogMapper;
import com.xiaohei.mbg.model.SysLog;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * @ClassName: WebAspect
 * @Description:
 * @Author: xiaohei on Date:2022/8/8 20:06
 * @Version: 1.0
 */
@Aspect
@Component
@RequiredArgsConstructor
public class WebAspect extends AbstractCtrl {

    private static final Logger log = LoggerFactory.getLogger(WebAspect.class);

    @Autowired
    private SysLogMapper sysLogMapper;
    
    @Pointcut("@annotation(com.xiaohei.common.annotation.LogAnnotation)")
    public void pointcut(){
        
    }
    
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point){
        Object result = null;
        long beginTime = System.currentTimeMillis();
        try {
            result = point.proceed();
        } catch (Throwable e) {
            //因为存在重复提交的Aspect, 重复提交时需要返回信息给前端
            //重复提交的Aspect没有执行的到point.proceed()
            //会导致webAspect类中的point.proceed(), 出现异常, 导致的情况就是前端没有消息
            throw new ApiException(ResultCodeEnum.REQUEST_ERROR);
        }
        long costTime = System.currentTimeMillis() - beginTime;
        saveLog(point,costTime);
        return result;
    }

    private void saveLog(ProceedingJoinPoint point, long costTime) {
        //通过 point 拿到方法签名
        MethodSignature methodSignature = (MethodSignature)point.getSignature();
        //通过方法签名拿到呗调用的方法
        Method method = methodSignature.getMethod();

        SysLog slog = new SysLog();
        LogAnnotation logAnnotation = method.getAnnotation(LogAnnotation.class);
        if (logAnnotation != null) {
            slog.setDescriptio(logAnnotation.value());
        }
        String className = point.getTarget().getClass().getName();
        String methodName = methodSignature.getName();
        slog.setMethod(className + "." + methodName + "()");

        // 获取方法的参数
        Object[] args = point.getArgs();
        LocalVariableTableParameterNameDiscoverer l = new LocalVariableTableParameterNameDiscoverer();
        String[] parameterNames = l.getParameterNames(method);
        if (args != null && parameterNames != null ) {
            StringBuilder param = new StringBuilder();
            for (int i = 0; i < args.length; i++) {
                param.append(" ").append(parameterNames[i]).append(":").append(args[i]);
            }
            slog.setParams(param.toString());
        }
        slog.setIp(StringAddressToUtils.StringToAddress(getClientIp()));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = "";
        name = authentication.getName();
        slog.setUserName(name);
        slog.setCostTime((int)costTime);
        slog.setCreateTime(new Date());
        sysLogMapper.insert(slog);
    }
}
