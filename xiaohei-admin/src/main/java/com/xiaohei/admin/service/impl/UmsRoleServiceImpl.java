package com.xiaohei.admin.service.impl;

import com.xiaohei.admin.dao.UmsRoleDao;
import com.xiaohei.admin.service.UmsRoleService;
import com.xiaohei.mbg.mapper.UmsRoleMapper;
import com.xiaohei.mbg.mapper.UmsRoleMenuRelationMapper;
import com.xiaohei.mbg.mapper.UmsRoleResourceRelationMapper;
import org.springframework.util.StringUtils;
import com.github.pagehelper.PageHelper;
import com.xiaohei.mbg.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: UmsRoleServiceImpl
 * @Description:
 * @Author: xiaohei on Date:2022/6/25 13:24
 * @Version: 1.0
 */
@Service
public class UmsRoleServiceImpl implements UmsRoleService {

    @Autowired
    private UmsRoleMapper roleMapper;
    @Autowired
    private UmsRoleResourceRelationMapper roleResourceRelationMapper;
    @Autowired
    private UmsRoleMenuRelationMapper roleMenuRelationMapper;
    @Autowired
    private UmsRoleDao roleDao;



    @Override
    public int create(UmsRole role) {
        role.setCreateTime(new Date());
        return roleMapper.insert(role);
    }

    @Override
    public int update(Integer id, UmsRole role) {
        role.setRoleId(id);
        return roleMapper.updateByPrimaryKeySelective(role);
    }

    @Override
    public int delete(List<Integer> ids) {
        UmsRoleExample example = new UmsRoleExample();
        example.createCriteria().andRoleIdIn(ids);
        return roleMapper.deleteByExample(example);
    }

    //查询所有角色
    @Override
    public List<UmsRole> list() {
        UmsRoleExample umsRoleExample = new UmsRoleExample();
        umsRoleExample.createCriteria().andStatusEqualTo(1);
        return roleMapper.selectByExample(umsRoleExample);
    }

    @Override
    public List<UmsRole> list(String keyword, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        UmsRoleExample example = new UmsRoleExample();
        if (!StringUtils.isEmpty(keyword)) {
            example.createCriteria().andNameLike("%" + keyword + "%");
        }
        return roleMapper.selectByExample(example);
    }


    @Override
    public List<UmsMenu> listMenu(Integer roleId) {
        return roleDao.getMenuListByRoleId(roleId);
    }

    @Override
    public List<UmsResource> listResources(Integer roleId) {
        return roleDao.getResourceListByRoleId(roleId);
    }

    //修改角色和用户的关系
    @Override
    public int allocMenu(Integer roleId, List<Integer> menuIds) {
        UmsRoleMenuRelationExample example = new UmsRoleMenuRelationExample();
        example.createCriteria().andRoleIdEqualTo(roleId);
        roleMenuRelationMapper.deleteByExample(example);
        //批量插入新关系
        for (Integer menuId : menuIds) {
            UmsRoleMenuRelation relation = new UmsRoleMenuRelation();
            relation.setRoleId(roleId);
            relation.setMenuId(menuId);
            roleMenuRelationMapper.insert(relation);
        }
        return menuIds.size();
    }

    @Override
    public int allocResource(Integer roleId, List<Integer> resourceIds) {
        UmsRoleResourceRelationExample example = new UmsRoleResourceRelationExample();
        example.createCriteria().andRoleIdEqualTo(roleId);
        roleResourceRelationMapper.deleteByExample(example);
        for (Integer resourceId : resourceIds) {
            UmsRoleResourceRelation relation = new UmsRoleResourceRelation();
            relation.setRoleId(roleId);
            relation.setResourceId(resourceId);
            roleResourceRelationMapper.insert(relation);
        }
        return resourceIds.size();
    }
}
