package com.xiaohei.quartz.service;

import com.xiaohei.common.utils.Result;
import com.xiaohei.mbg.model.SysQuartz;

import java.util.List;

public interface SysQuartzService {

    //crud新建定时任务
    Result add(SysQuartz sysQuartz,String createName);

    //创建定时任务
    void schedulerAdd(String className, String cronExpression, String param);

    //删除定时任务
    void schedulerDelete(String className);

    //启动项目自动加载定时任务
    void schedulerStart();

    int delete(String className);

    //停止指定的定时任务
    void schedulerStop(String className);

    //启动指定的定时任务
    void schedulerStart(String className);

    //查询指定定时Name查任务
    List<SysQuartz> select(SysQuartz sysQuartz);

    //按照class询定时任务
    List<SysQuartz> selectByClassName(String ClassName);

    //模糊查询定时任务
    List<SysQuartz> selectByFuzzy(SysQuartz sysQuartz, Integer pageSize, Integer pageNum);

    //更新定时任务
    int update(SysQuartz sysQuartz);

    int updateStatus(String className, Boolean status);


    //判断cron合法性
    void isValidExpression(String cron);




}
