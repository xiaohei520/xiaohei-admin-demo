package com.xiaohei.admin.service;

import com.xiaohei.admin.dto.UmsResourceNode;
import com.xiaohei.mbg.model.UmsResource;

import java.util.List;

public interface UmsResourceService {
    int create(UmsResource UmsResource);

    int update(Integer id, UmsResource UmsResource);

    UmsResource getItem(Integer id);

    int delete(Integer id);

    List<UmsResource> list(String nameKeyword, String urlKeyword,Integer categoryId, Integer pageSize, Integer pageNum);

    List<UmsResource> listAll();

    List<UmsResourceNode> treeList();
}

